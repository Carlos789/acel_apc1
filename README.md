# Acel_APC

Poryecto de un acelerómetro, comunicación APC
Estos son los comandos que tengo

### Comenzar medición por tiempo:

    0xFD 0x09 0x00 (broadcast)  0x03 (comando) tiempo (3 bytes de tiempo) mask (1 byte) Bloque



### Comenzar medición por trigger:

    0xfd 11 0x00(broadcast) 0x04 master (1 byte) trigger (1byte) tiempo (3bytes) 1 mask (1byte)

### Obtener ID:

    0xFD 0x04 ID 0xA6  		Rta : 0xFE 0x05  ID 0xA6 0x6A 
    

### Parar Medición:

    0xFD 0x04 ID 0x48		Rta: 0xFE 0x06 ID 0x48 BH BL  (BH y BL bytes alto y bajo de las paginas grabadas)


### Setear escala:

    0xfd 0x05 id 0xee escala(1byte) 


#### Borrar Memoria:

    0xFD 0x04 ID 0x53		Rta: 0xFE 0x04 ID 0x53


#### Unlock Memoria:

    0xFD 0x04 ID 0x56		Rta: 0xFE 0x04 ID 0x56

#### Leer página:

    0xfd 0x06 id 0xff pagina (2bytes)               

    0xFD 0x06 ID 0x58  DIREH DIREL                     Rta    0xFE 0x04 ID 0x58  más 256 valores


#### Comenzar Medición:

    0xFD 0x05 ID 0x47 Bloque(Lectura)                          Rta 0xFE 0x04 ID 0x47 

## Comandos propios

### Lectura de datos almacenados en memoria serial

    0xFD 0x06 ID 0x49 DireH DireL			Rta 0xFE 0x04 ID 0x49 y 256 valores de la página
    Con DireH y L dirección de página 
 
### Borrado de bloques de la memoria serial

      0xFD 0x06 ID 0x5 Bloque 
Rta   0xFE 0x04 ID 0x5 Bloque
Donde Bloque  va de 0 a 5 para cada medici�n, y con 0xFF borra todos los bloques menos los 64 KBytes iniciales 
    Con DireH y L dirección de página 

### Cambio del ID del dispositivo

      0xFD 0x04 0xff  ID 
Rta   No hay repuesta
El valor del ID se almacena en la direccion 0 0 0 de la memoria serial
El cual se lee luego de un reset del micro