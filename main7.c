/*
 *      Código para experiencia con acelerómetro FXLS8471 
 * Se agrega interrupciones por INT0  , corresponde al pin RB7
 * FCY=60MIPS
 *Fecha:12/11/15
 */
#include <p33EP512GM304.h>
#include "MSPI1.h"
#include "IMU.h"
#include "RF.h" //Libreria nRF24l01 
#include "MemSPI.h"
#include "UARTSD.h"
#include <stdio.h> /* para usar  printf */
#include <string.h>
#define FCY 60000000UL//
#include <libpic30.h>
#include <uart.h>
#include <math.h>
#include <stdio.h> /* para usar  printf */
#include <pps.h>

/*
 * Para memoria SD 
 */
#include "fileio.h"
#include "sd_spi.h"
extern FILEIO_SD_DRIVE_CONFIG sdCardMediaParameters;
const FILEIO_DRIVE_CONFIG gSdDrive =
{
    (FILEIO_DRIVER_IOInitialize)FILEIO_SD_IOInitialize,                      // Function to initialize the I/O pins used by the driver.
    (FILEIO_DRIVER_MediaDetect)FILEIO_SD_MediaDetect,                       // Function to detect that the media is inserted.
    (FILEIO_DRIVER_MediaInitialize)FILEIO_SD_MediaInitialize,               // Function to initialize the media.
    (FILEIO_DRIVER_MediaDeinitialize)FILEIO_SD_MediaDeinitialize,           // Function to de-initialize the media.
    (FILEIO_DRIVER_SectorRead)FILEIO_SD_SectorRead,                         // Function to read a sector from the media.
    (FILEIO_DRIVER_SectorWrite)FILEIO_SD_SectorWrite,                       // Function to write a sector to the media.
    (FILEIO_DRIVER_WriteProtectStateGet)FILEIO_SD_WriteProtectStateGet,     // Function to determine if the media is write-protected.
};
// Declare a state machine for our device
typedef enum
{
    DEMO_STATE_NO_MEDIA = 0,
    DEMO_STATE_MEDIA_DETECTED,
    DEMO_STATE_DRIVE_MOUNTED,
    DEMO_STATE_DONE,
    DEMO_STATE_FAILED
} DEMO_STATE;

_FICD(ICS_PGD3 & JTAGEN_OFF);// Para hacer debuging por el puerto 2
_FOSCSEL(FNOSC_FRC);
_FOSC(FCKSM_CSECMD & OSCIOFNC_OFF & POSCMD_XT);
//_FOSCSEL(FNOSC_LPRC);
_FWDT(FWDTEN_OFF);
_FPOR(ALTI2C2_OFF) ;//  se usa I2C alternativo pines
//#pragma config FNOSC = LPRC  // default System clock = LPRC
/************************ HEADERS MIWI****************************************/

/* ** ********************Funciones****************************************************** ** */
void Config1(void);// Configuraciób de puertos y otros
void IntInit(void);
void EnviarTX(void);    //Funcion para enviar USART1
void EnviarTX2(void);  //Funcion para enviar USART2
void EnviarMMA(void);
void EnviarMMA2(void);
void MMA8471Activo(void);
void MMA8471DesActivo(void);
void MMA8471Reset(void);
void get_int_hex( unsigned int b, char *StrHexa );//Funcion para pasar de int Hexa a string
void GetTimestamp (FILEIO_TIMESTAMP * timeStamp);//Es para forzar fecha y hora en archivos SD
 void readotro(char cod,char reg1, int count);
 void ClearBloq( unsigned char bloq);//Borrar bloque memoria serial
void __attribute__((interrupt, no_auto_psv)) _INT0Interrupt(void);// Rutina de interrupcion INT0
void __attribute__((interrupt, no_auto_psv)) _U1RXInterrupt( void );//Rutina de interrupcion
void __attribute__((interrupt, no_auto_psv)) _U1TXInterrupt( void );//Rutina de interrupcion;
void __attribute__((interrupt, no_auto_psv)) _SPI2Interrupt( void );//Rutina interrupcion SPI2
void __attribute__((interrupt, no_auto_psv)) _U2RXInterrupt( void );//Rutina de interrupcion USART2
void __attribute__((interrupt, no_auto_psv)) _U2TXInterrupt( void );//Rutina de interrupcion; USART2
/* ****************************************************************************************/


#define LED1	PORTBbits.RB0
/* ** *********************************************************************************** ** */
unsigned char sensor=3; // Para este caso  cambiar para otros sensores
char  cachito[10];//Vector para nRF
unsigned char Vector[6];//Vector para almacenar  Aceleración
char VectorRS[256];//Por ahora son 256 bytes
unsigned char VectorMEM[256];//
unsigned char PageH,PageL;//es para almacenar en diferentes paginas en la memoria
char VectorMMA[192];//
float VOffset[3];
int j, Trama;
int Cuenta=0;// Registro de numero de vectores leidos
unsigned char ID , ID1;
int OUT_X = 0;
int OUT_Y = 0;
int OUT_Z = 0;
char buffer1[];
unsigned char datt;
unsigned char CTRL_REG1,Vcuenta = 0;
float temp = 0.0;
unsigned char orden,Kbhit, Kbhit2,parar,Nbytes,cancelar;
unsigned char ID; // Identificacion forzada =1
int Dato;//Variable de lectura del acelerómetro
char INT_SOURCE;
unsigned char Dire,DireH,DireL,Valor;
int datos[256]={0}; //Vector de datos a escribir en la memoria
char aux, i;   //registros para nRF
static const char nybble_chars[] = "0123456789ABCDEF";//Vector constante de los simbolos hexadecimales
char Str[20];//Vector para tener ascii de los enteros de las 3 aceleraciones
char Str1[5];//Vector para tener ascii de los enteros de una aceleracion
unsigned int ENTERO; //Es para juntar las dos partes de la aceleracion en un entero 
/***********************Registro de Jaime**********************************/
union {
    unsigned char aa[3];
    signed long vlong;
} char2long;

long muestras_P, cuenta_M;
unsigned char Master, Trigger, muestras_C, indice_C, g;
char cachito2[32];
signed char cachito2signed[3];
signed char circularsigned[3];
char circular[512];
int S,V;
unsigned char UltimaH = 0;
unsigned char UltimaL = 0;
unsigned char BloqueBK;
//***************SD******************
char tiempo[6]; //Vector para registro de tiempo para crear archivo en SD
char Name[5];//Vector para almacenar nombre archivo SD
unsigned char PageHi,PageLi;//es para almacenar el inicio en diferentes paginas en la memoria
unsigned char Bloque, Sector;// Saber que obloque o toda la memoria hay que borrar
unsigned char Bloque1;
int main (void){
 FILEIO_OBJECT file;   
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////// CONDICIONES INICIALES y DECLARACIONES DE VARIABLES ////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
        CS_FX=1;  //Levanto el CS del FX
        CS_M=1;  //Levanto el CS de la memoria serial 
    Config1();// Configuración de USART y mapeo de pines
   
   __delay_us(50);// A modo de prueba
  
   OpenSPI1();//Configuro SPI 1
  // printf("Inicio Placa DSPIC\n");
    __delay_ms(5);

   __delay_ms(1);
    OpenSPI2(); //Configuro SPI 2
   __delay_ms(10);
  
    OpenSPI3(); //Configuro SPI 3
    __delay_ms(10);

    sensor=LeerDato (0,0,0);// Leo el ID DEL DISPOSITIVO
    sensor=3; //Se coloca por ahora fijo
/*************************************
     configuraciones iniciales del nrf24L01+
     */
 // RFWRITE(W_RF_SETUP,0b00001000);//bajamos la potencia del transmisor -18dB
    //U1TXREG = RFBREAD(R_RX_PAYLOAD);
    
/***********************************************************************
 Configuracion de la IMU
  ************************************************************************/

IMUWRITE(0x2A,0x0);// MMA se pone en modo des-activo
__delay_ms(100); // tiempo necesario antes de iniciar otra comunicacion, en el caso de escritura
IMUWRITE(0x2A,0x1);// MMA se pone en modo activo 
 UnlockMem ();  //Global Block Protection Unlock
/*********************Interrpción***************************************************/
           //Rutina de inicialización de interrupciones
        IntInit();
/* ***************I2C Inicialización************************************************/
//        Dato=IMUBREAD(cmdW,0x1A);//Leo registro 26 Estado de Interrupciones
        /* de paso bajo la bandera de dato listo en la IMU*/
      Kbhit=0;// Limpio bandera USART1
      Kbhit2=0;// Limpio bandera USART2
      parar=0;
      while(1)// codigo de procesamiento
            {
             if(Kbhit==1)
          {
             Kbhit=0;
        switch(orden)
        {
            
        case 0x03: // Medicion por tiempo    
         //borramos la memoria
        //ChipErase();
         switch(Bloque)
        {
                case 0x00:// Medicion 1
                    PageL=0;
                    PageH=1;
                    break;
                 
                case 0x01:// Medicion 2
                  PageL=0;
                  PageH=23;
                  break;
                 
                case 0x02:// Medicion 3
                 PageL=0;
                 PageH=45;
                 break;
                 
                case 0x03:// Medicion 4
                PageL=0;
                PageH=67;
                 break;
                 
                case 0x04:// Medicion 5
                 PageL=0;
                 PageH=89;
                 break;
                 
                case 0x05:// Medicion 6
                PageL=0;
                PageH=111;
                 break;
         }
        muestras_P = char2long.vlong;
         MMA8471DesActivo();
                    __delay_ms(1);
                    IMUWRITE(0x09, 0x00); //Fifo des habilitada
                    __delay_us(5);
                    IMUWRITE(0x2D, 0x01); //en CTRL_REG4 pongo INT_EN_DRDY=1
                    __delay_us(5);
                    MMA8471Activo();
                    __delay_us(5);
                    cuenta_M=0;
                    Vcuenta=0;
//                    PageL=0;
//                    PageH=0;
                    while( (cuenta_M < muestras_P)&& parar!=0)//almacenamos las aceleraciones muestras_P veces
                    {
                        INT_SOURCE = IMUBREAD(0x0C); //leo INT_SOURCE
                        if ((INT_SOURCE & 0x01) == 0x01)// pregunto si hay interrupcion por INT_EN_DRDY
                        {
                            for (j = 0; j < 6; j++) {
                                VectorMEM[j + Vcuenta * 6] = IMUBREAD(j + 1); //
                                __delay_us(5);
                            }
                            Vcuenta++;
                            cuenta_M++;
                            if (Vcuenta == 42)//Representa 252 bytes quedan 4 bytes libre
                            {
                                VectorMEM[252] = PageH; //almaceno el valor alto de la pagina
                                VectorMEM[253] = PageL;
                                EscribirPagina(PageH, PageL, 0, VectorMEM); // escribo la pagina con datos del acelerometro       
                                PageL = PageL + 1;
                                if (PageL == 0) {
                                    PageH = PageH + 1;
                                }
                                Vcuenta = 0;
                            }
                        }
                    }
                    Bloque1= (Bloque+1)*0x10;
                     EscribirDato (0,Bloque1,0, PageL);//Prueba eb la pagima 0x0100XX
                      __delay_ms(2);                     
                     EscribirDato (0,Bloque1,1, PageH);//Prueba eb la pagima 0x0100XX
                    
        break;
        
            case 0x04:  // Trigger
            //borramos la memoria
            //ChipErase();
            switch(Bloque)
        {
                case 0x00:// Medicion 1
                    PageL=0;
                    PageH=1;
                    BloqueBK=PageH;
                    break;
                 
                case 0x01:// Medicion 2
                  PageL=0;
                  PageH=23;
                  BloqueBK=PageH;
                  break;
                 
                case 0x02:// Medicion 3
                 PageL=0;
                 PageH=45;
                 BloqueBK=PageH;
                 break;
                 
                case 0x03:// Medicion 4
                PageL=0;
                PageH=67;
                BloqueBK=PageH;
                 break;
                 
                case 0x04:// Medicion 5
                 PageL=0;
                 PageH=89;
                 BloqueBK=PageH;
                 break;
                 
                case 0x05:// Medicion 6
                PageL=0;
                PageH=111;
                BloqueBK=PageH;
                 break;
         }
            muestras_P = char2long.vlong;
            //conf_recepcion_slave1 (); // ver que hace por ahora no se usa   
                     MMA8471DesActivo();
                    __delay_ms(1);
                    IMUWRITE(0x09, 0x00); //Fifo des habilitada
                    __delay_us(5);
                    IMUWRITE(0x2D, 0x01); //en CTRL_REG4 pongo INT_EN_DRDY=1
                    __delay_us(5);
                    MMA8471Activo();
                    __delay_us(5);
                    if (Master ==1 )//yo soy el maestro 
                    {
                        INT_SOURCE = IMUBREAD(0x0C); //leo INT_SOURCE
                        while ((INT_SOURCE & 0x01) == 0x01) {// pregunto si hay interrupcion por INT_EN_DRDY
                            INT_SOURCE = IMUBREAD(0x0C);
                        } //leo INT_SOURCE
                        for (j = 0; j < 6; j++) {
                            cachito2[j] = IMUBREAD(j + 1);
                            __delay_us(5);
                        }
                        cachito2signed[0] = cachito2[0]; //signed MSB x
                        cachito2signed [1] = cachito2[2]; //signed MSB y
                        cachito2signed[2] = cachito2[4]; //signed MSB Z
                   }
                    parar = 2;
                    Vcuenta = 0;
                    cancelar=1;  // para detectar disparo por trigger
                    //almacenamiento ciclico de mediciones
                    while (parar != 0 && cancelar!=0 ) {
                        INT_SOURCE = IMUBREAD(0x0C); //leo INT_SOURCE
                        if ((INT_SOURCE & 0x01) == 0x01)// pregunto si hay interrupcion por INT_EN_DRDY
                        {
                            for (j = 0; j < 6; j++) {
                                __delay_us(5);
                                circular[j + Vcuenta * 6] = IMUBREAD(j + 1); //guardo la cola en la variable circular                               
                         }
                            
                            if (Master ==1 ) {
                                //combinamos los datos medidos en signed int y lo comparamos con el almacenado
                                circularsigned[0] = circular[ Vcuenta * 6]; //MSB x estado actual
                                circularsigned[1] = circular[2 + Vcuenta * 6]; //MSB y estado actual
                                circularsigned[2] = circular[4 + Vcuenta * 6]; //MSB z estado actual
                                
                             if (((circularsigned[0] > cachito2signed[0] + Trigger) || (circularsigned[0] < cachito2signed[0] - Trigger)) || ((circularsigned[1] > cachito2signed[1] + Trigger) || (circularsigned[1] < cachito2signed[1] - Trigger)) || ((circularsigned[2] > cachito2signed[2] + Trigger) || (circularsigned[2] < cachito2signed[2] - Trigger))) {
                                    cancelar = 0;
                                    VectorRS[0] = 0xFD;
                                    VectorRS[1] = 0x03; // comando para que se detengan los demas sensores
                                    VectorRS[2] = 0x01;
                                  }
                                
                            }
                    
                             Vcuenta = Vcuenta + 1;
                            if (Vcuenta > muestras_C * 42) {//reinicio  Vcuenta
                                Vcuenta = 0;
                             }
                        } }
                     indice_C = Vcuenta; //guardamos donde guardamos el ultimo dato en la memoria circular
                    Vcuenta = 0;
                    PageL = muestras_C; //empezamos a guardar nuevas mediciones dejando espacio para la cola
                    //PageH = 0;
                    cuenta_M = 0;
                    //UnlockMem();
                    while ((cuenta_M < muestras_P) && parar!=0)//almacenamos las aceleraciones muestras_P veces
                    {
                        INT_SOURCE = IMUBREAD(0x0C); //leo INT_SOURCE
                        if ((INT_SOURCE & 0x01) == 0x01)// pregunto si hay interrupcion por INT_EN_DRDY
                        {
                            for (j = 0; j < 6; j++) {
                                VectorMEM[j + Vcuenta * 6] = IMUBREAD(j + 1); //
                                __delay_us(5);
                            }
                            Vcuenta = Vcuenta + 1;
                            cuenta_M++;
                            if (Vcuenta == 42)//Representa 252 bytes quedan 4 bytes libre
                            {
                                VectorMEM[252] = PageH; //almaceno el valor alto de la pagina
                                VectorMEM[253] = PageL;
                                EscribirPagina(PageH, PageL, 0, VectorMEM); // escribo la pagina con datos del acelerometro       
                                PageL = PageL + 1;
                                if (PageL == 0) {
                                    PageH = PageH + 1;
                                }
                                Vcuenta = 0;
                            }
                        }
                    }
                    UltimaL=PageL;
                    UltimaH=PageH;
                    //escribimos la memoria ciclica en la mem flash
                    PageL = 0;
                    PageH = BloqueBK;
                    g = 0;
                    V = Vcuenta;
                    //UnlockMem();
                    for (S = 0; S <= (muestras_C * 42); S++) {
                        memcpy(VectorMEM + g * 6, circular + V * 6, 6);
                        g++;
                        V++;
                        //completamos una pagina y la grabamos en memoria
                        if (g == 42) {
                            g = 0;
                            VectorMEM[252] = PageH; //almaceno el valor alto de la pagina
                            VectorMEM[253] = PageL;
                            EscribirPagina(PageH, PageL, 0, VectorMEM); // escribo la pagina con datos del acelerometro 
                            PageL = PageL + 1;
                            if (PageL == 0) {//cuando se desborda PageL suma 1 a PageH
                                PageH = PageH + 1;
                            }
                        }
                        if (V == (muestras_C * 42)) {
                            V = 0;
                        }
                    }
                    PageL=UltimaL;
                    PageH=UltimaH;
                    Bloque1= (Bloque+1)*0x10;
                     EscribirDato (0,Bloque1,0, PageL);//Prueba eb la pagima 0x0100XX
                      __delay_ms(2);                     
                     EscribirDato (0,Bloque1,1, PageH);//Prueba eb la pagima 0x0100XX
                        break;
        
      case ArchSD:// 0x10 Generar archivo SD 
                    VectorRS[0] = 0xFE;
                    VectorRS[1] = 0x5; // 4 valores
                    VectorRS[2] = ID;
                    VectorRS[3] = orden;
                    // Register the GetTimestamp function as the timestamp source for the library.
                    FILEIO_RegisterTimestampGet(GetTimestamp);
                    ENTERO = VectorMEM[6 * j]*256 + VectorMEM[6 * j + 1];
                    get_int_hex(ENTERO, Name); //paso de int a ASCII en Hexa
                    if (FILEIO_Initialize() == -1) {
                        VectorRS[4] = 0x01; //error  de inicializacion de archivo
                        EnviarTX();
                        break; //finaliza proceso de grabar archivo en SD e indica error al soft
                    }
                    if (FILEIO_MediaDetect(&gSdDrive, &sdCardMediaParameters) == false) {
                        VectorRS[4] = 0x02; //SD no se detecta
                        EnviarTX();
                        break; //finaliza proceso de grabar archivo en SD e indica error al soft
                    }
                    if (FILEIO_DriveMount('A', &gSdDrive, &sdCardMediaParameters) != FILEIO_ERROR_NONE)
                        //Mount the SD card as a drive with drive letter 'A'    
                    {
                        VectorRS[4] = 0x03; //SD no se monta
                        EnviarTX();
                        break; //finaliza proceso de grabar archivo en SD e indica error al soft
                    }


                    // Open a file called  and store the file data in the FILEIO_OBJECT "file."
                    // This file is being opened with the CREATE flag, which will allow the library to create the file if it doesn't already exist, the
                    // TRUNCATE flag, which will truncate the file to a 0-length file if it already exists, the WRITE flag, which will allow us to
                    // write to the file, and the READ flag, which will allow us to read from it.

                    if (Valor == 0)//  comienzo pagina 0
                    {
                        PageH = 1;
                        if (FILEIO_Open(&file, "LECT1.CSV", FILEIO_OPEN_WRITE | FILEIO_OPEN_APPEND | FILEIO_OPEN_CREATE) == FILEIO_RESULT_FAILURE) {
                            VectorRS[4] = 0x04; //El archivo no se pudo crear
                            EnviarTX();
                            break; //finaliza proceso de grabar archivo en SD e indica error al soft
                        }
                    }

                    if (Valor == 1) {
                        PageH = 23; //  comienzo pagina
                        if (FILEIO_Open(&file, "LECT2.CSV", FILEIO_OPEN_WRITE | FILEIO_OPEN_APPEND | FILEIO_OPEN_CREATE) == FILEIO_RESULT_FAILURE) {
                            VectorRS[4] = 0x04; //El archivo no se pudo crear
                            EnviarTX();
                            break; //finaliza proceso de grabar archivo en SD e indica error al soft
                        }
                    }

                    if (Valor == 2)//  comienzo pagina 0x2000
                    {
                        PageH = 45;
                        if (FILEIO_Open(&file, "LECT3.CSV", FILEIO_OPEN_WRITE | FILEIO_OPEN_APPEND | FILEIO_OPEN_CREATE) == FILEIO_RESULT_FAILURE) {
                            VectorRS[4] = 0x04; //El archivo no se pudo crear
                            EnviarTX();
                            break; //finaliza proceso de grabar archivo en SD e indica error al soft
                        }
                    }

                    if (Valor == 3)//  comienzo pagina 0x3000
                    {
                        PageH = 67;
                        if (FILEIO_Open(&file, "LECT4.CSV", FILEIO_OPEN_WRITE | FILEIO_OPEN_APPEND | FILEIO_OPEN_CREATE) == FILEIO_RESULT_FAILURE) {
                            VectorRS[4] = 0x04; //El archivo no se pudo crear
                            EnviarTX();
                            break; //finaliza proceso de grabar archivo en SD e indica error al soft
                        }
                    }

                    if (Valor == 4)//  comienzo pagina 0x4000
                    {
                        PageH = 89; //  
                        if (FILEIO_Open(&file, "LECT5.CSV", FILEIO_OPEN_WRITE | FILEIO_OPEN_APPEND | FILEIO_OPEN_CREATE) == FILEIO_RESULT_FAILURE) {
                            VectorRS[4] = 0x04; //El archivo no se pudo crear
                            EnviarTX();
                            break; //finaliza proceso de grabar archivo en SD e indica error al soft
                        }
                    }

                    if (Valor == 5)//  comienzo pagina 0x5000
                    {
                        PageH = 111; //  
                        if (FILEIO_Open(&file, "LECT6.CSV", FILEIO_OPEN_WRITE | FILEIO_OPEN_APPEND | FILEIO_OPEN_CREATE) == FILEIO_RESULT_FAILURE) {
                            VectorRS[4] = 0x04; //El archivo no se pudo crear
                            EnviarTX();
                            break; //finaliza proceso de grabar archivo en SD e indica error al soft
                        }
                    }
                    VectorRS[4] = 0x00; //El archivo se creo con exito
                    EnviarTX();
    IFS0bits.U1RXIF = 0;// Limpio la bandera de la interrupción
	IEC0bits.U1RXIE = 0;//Deshabilito la interrupción por recepción RX
                    

                   
                    DireL = 0;
                    for (Cuenta = 0; Cuenta < 0x1600; Cuenta++)// leer 1,6Mbytes
                        /*
                        8 Mbytes de la memoria serial representa 0x8000 paginas de 256 byts cada uno 
                         La memoria tiene 128 sectores de 64 kbytes
                        ( 8 lecturas de 1 Mbytes  es 0x1000 paginass (decimal 4096)
                         * quedando cada medicion de 0x1000 pagina, que representa mas de 3,5 minutos
                         */ {
                        DireH = DireH +(Cuenta >> 8);
                        DireL = Cuenta;
                        LeerPagina(DireH, DireL, 0, VectorMEM); //
                        for (j = 0; j < 42; j++)// Paso a ASCII los 42 valores
                        {
                            ENTERO = VectorMEM[6 * j]*256 + VectorMEM[6 * j + 1];
                            get_int_hex(ENTERO, Str1); //paso de int a ASCII en Hexa
                            strcat(Str, Str1);
                            ENTERO = VectorMEM[6 * j + 2]*256 + VectorMEM[6 * j + 3];
                            get_int_hex(ENTERO, Str1); //paso de int a ASCII en Hexa
                            strcat(Str, Str1);
                            ENTERO = VectorMEM[6 * j + 4]*256 + VectorMEM[6 * j + 5];
                            get_int_hex(ENTERO, Str1); //paso de int a ASCII en Hexa
                            strcat(Str, Str1);
                            strcat(Str, "\r\n");
                            FILEIO_Write(Str, 1, 17, &file); //escribo un renglon en el archivo
                            Str[0] = 0; //Forzamos en primer caracte en nulo
                        }
                        //        ENTERO=VectorMEM[252]*256+VectorMEM[253];    
                        //        get_int_hex( ENTERO,Str1 );//paso de int a ASCII en Hexa
                        //        strcat(Str, Str1 );
                        //        strcat(Str,"\r\n");
                        //        FILEIO_Write (Str, 1, 6, &file)  ;  //escribo un renglon en el archivo
                        //        Str[0]=0; //Forzamos en primer caracte en nulo
                        //       __delay_ms(10);//prueba
                    }

                    FILEIO_Close(&file);
        IFS0bits.U1RXIF = 0;// Limpio la bandera de la interrupción
        IEC0bits.U1RXIE = 1;//Habilito la interrupción por recepción RX
                    
                    break;    
        
            case 0x05://Comando borrar bloques de memoria
                
                switch(Bloque)
                {   case 0x00:// Medicion 1
                    for (j=1;j<23;j++)
                    { ClearBloq( j);}
                    __delay_ms(100);
                    VectorRS[0]=0xFE;
                    VectorRS[1]=0x05;// 5 valores
                    VectorRS[2]= ID;
                    VectorRS[3]= orden;
                    VectorRS[4]= Bloque;
                    EnviarTX();
                    break;
                 
                case 0x01:// Medicion 2
                  for (j=23;j<45;j++)
                 { ClearBloq( j);}          
                  __delay_ms(100);
                  VectorRS[0]=0xFE;
                    VectorRS[1]=0x05;// 5 valores
                    VectorRS[2]= ID;
                    VectorRS[3]= orden;
                    VectorRS[4]= Bloque;
                    EnviarTX();
                  break;
                 
                case 0x02:// Medicion 3
                 for (j=45;j<67;j++)
                 { ClearBloq( j);}       
                  __delay_ms(100);
                  VectorRS[0]=0xFE;
                    VectorRS[1]=0x05;// 5 valores
                    VectorRS[2]= ID;
                    VectorRS[3]= orden;
                    VectorRS[4]= Bloque;
                    EnviarTX();
                 break;
                 
                case 0x03:// Medicion 4
                 for (j=68;j<89;j++)
                 { ClearBloq( j);}          
                  __delay_ms(100);
                  VectorRS[0]=0xFE;
                    VectorRS[1]=0x05;// 5 valores
                    VectorRS[2]= ID;
                    VectorRS[3]= orden;
                    VectorRS[4]= Bloque;
                    EnviarTX();
                 break;
                 
                case 0x04:// Medicion 5
                 for (j=89;j<111;j++)
                 { ClearBloq( j);}      
                  __delay_ms(100);
                  VectorRS[0]=0xFE;
                    VectorRS[1]=0x05;// 5 valores
                    VectorRS[2]= ID;
                    VectorRS[3]= orden;
                    VectorRS[4]= Bloque;
                    EnviarTX();
                 break;
                 
                case 0x05:// Medicion 6
                 for (j=111;j<133;j++)
                 { ClearBloq( j);}          
                  __delay_ms(100);
                  VectorRS[0]=0xFE;
                    VectorRS[1]=0x05;// 5 valores
                    VectorRS[2]= ID;
                    VectorRS[3]= orden;
                    VectorRS[4]= Bloque;
                    EnviarTX();
                 break;
                    
                case 0xFF:// borrar toda la memoria
                for (j=1;j<133;j++)
                 { ClearBloq( j);}          
                  __delay_ms(100);  
                 __delay_ms(100);
                 VectorRS[0]=0xFE;
                    VectorRS[1]=0x05;// 5 valores
                    VectorRS[2]= ID;
                    VectorRS[3]= orden;
                    VectorRS[4]= Bloque;
                    EnviarTX();
                break;
            
                }
                break;
         
        case 0x07:// seleccionar bloqueFalta ID
        VectorRS[0] = 0xFE;
        VectorRS[1] = 0x05;
        VectorRS[2]= ID;
        VectorRS[3]= orden;
        VectorRS[4]= Bloque;
        EnviarTX();
        break;        
        
        case 0x08://Comando borrar Sectores de memoria
               switch(Sector)
               {
                   
                 case 0x00:// Medicion 1
                   SectorErase(0,0x10,0 );
                    __delay_ms(100);
                    VectorRS[0]=0xFE;
                    VectorRS[1]=0x05;// 5 valores
                    VectorRS[2]= ID;
                    VectorRS[3]= orden;
                    VectorRS[4]= Sector;
                    EnviarTX();
                    break;
                 
                case 0x01:// Medicion 2
                    SectorErase(0,0x20,0 );
                    __delay_ms(100);
                    VectorRS[0]=0xFE;
                    VectorRS[1]=0x05;// 5 valores
                    VectorRS[2]= ID;
                    VectorRS[3]= orden;
                    VectorRS[4]= Sector;
                    EnviarTX();
                    break;
                                
                case 0x02:// Medicion 3
                    SectorErase(0,0x30,0 );
                    __delay_ms(100);
                    VectorRS[0]=0xFE;
                    VectorRS[1]=0x05;// 5 valores
                    VectorRS[2]= ID;
                    VectorRS[3]= orden;
                    VectorRS[4]= Sector;
                    EnviarTX();
                    break;
               
                 
                case 0x03:// Medicion 4
                    SectorErase(0,0x40,0 );
                    __delay_ms(100);
                    VectorRS[0]=0xFE;
                    VectorRS[1]=0x05;// 5 valores
                    VectorRS[2]= ID;
                    VectorRS[3]= orden;
                    VectorRS[4]= Sector;
                    EnviarTX();
                    break;
                                
                case 0x04:// Medicion 5
                    SectorErase(0,0x50,0 );
                    __delay_ms(100);
                    VectorRS[0]=0xFE;
                    VectorRS[1]=0x05;// 5 valores
                    VectorRS[2]= ID;
                    VectorRS[3]= orden;
                    VectorRS[4]= Sector;
                    EnviarTX();
                    break;
               
                 
                case 0x05:// Medicion 6
                    SectorErase(0,0x60,0 );
                    __delay_ms(100);
                    VectorRS[0]=0xFE;
                    VectorRS[1]=0x05;// 5 valores
                    VectorRS[2]= ID;
                    VectorRS[3]= orden;
                    VectorRS[4]= Sector;
                    EnviarTX();
                    break;
               
                    
                case 0xFF:// borrar toda la memoria
                    SectorErase(0,0,0 );
                    __delay_ms(100);
                    VectorRS[0]=0xFE;
                    VectorRS[1]=0x05;// 5 valores
                    VectorRS[2]= ID;
                    VectorRS[3]= orden;
                    VectorRS[4]= Sector;
                    EnviarTX();
                    break;
               }
                break;
                
            case 0x20:// Falta ID
        U1TXREG = RFBREAD(R_RX_PAYLOAD);
        RFREAD(R_RX_ADDR_P1);
        VectorRS[0] = 0xFE;
        VectorRS[1] = 0x07;
        VectorRS[2]= cachito[0];
        VectorRS[3]= cachito[1];
        VectorRS[4]= cachito[2];
        VectorRS[5]= cachito[3];
        VectorRS[6]= cachito[4];
        EnviarTX();
        break;
        
        case RECEPTOR:// Falta ID
        U1TXREG = 0xd0;
        aux=RF_RX();               
        VectorRS[0] = 0xFE;
        VectorRS[1] = 0x07;
        VectorRS[2]= cachito[0];
        VectorRS[3]= cachito[1];
        VectorRS[4]= cachito[2];
        VectorRS[5]= cachito[3];
        VectorRS[6]= cachito[4];
//                    VectorRS[2]=aux;
//                    VectorRS[3]=RFBREAD(R_RX_PAYLOAD);
//                    VectorRS[4]=RFBREAD(R_STATUS);
        EnviarTX();
        break;
                
        case TRANSMISOR:// Falta ID
        U1TXREG = 0xE0;
        RF_TX(0Xdd);
        break;
        
        case LEER_REGISTROS:      //case 0x41 LEER_REGISTROS:
        VectorRS[0]=0xFE;
        VectorRS[1]=0x36;// 54 valores
        VectorRS[2]=ID;// 
        VectorRS[3]= orden;
        for(j=0;j<0x32;j++)
        {
        VectorRS[j+4]=IMUBREAD(j);// Leo todos los registros
         __delay_us(5);
        }
        EnviarTX();
        break;

        case LEER_ACELERACIONES:        //case 0x42  LEER_REGISTROS:
        VectorRS[0]=0xFE;
        VectorRS[1]=0x0A;// 10 valores
        VectorRS[2]=ID;
        VectorRS[3]= orden;
        for(j=1;j<7;j++)
        {
        VectorRS[j+3]=IMUBREAD(j);//
        __delay_us(5);
        }
        EnviarTX();
        break;
        
        case ESC_REGISTRO:                //case 0x43 escribir_REGISTROS:
        MMA8471DesActivo();
        __delay_ms(1);
        IMUWRITE(Dire,Valor);
        __delay_ms(1);
        MMA8471Activo();
        VectorRS[0]=0xFE;
        VectorRS[1]=0x04;// 4 valores
        VectorRS[2]= ID;
        VectorRS[3]= orden;
        EnviarTX();
        break;

        case LECT_FIFO:        //CASE 0x44 Leer aceleraciones de la memoria FIFO
        parar=1;
        MMA8471DesActivo();
        __delay_ms(1);
        IMUWRITE(0x09,0x80);//Fifo circular y 32 muestras
        __delay_ms(1);
        IMUWRITE(0x2D,0x40);//en CTRL_REG4 pongo INT_EN_FIFO=1
        __delay_ms(1);
        IMUWRITE(0x2E,0x40);//en CTRL_REG5 pongo Interrupt is routed to INT1 pin
        __delay_ms(1);
       IMUWRITE(0x2C,0x82); // en CTRL_REG3 FIFO, interrupt active high (rising event)
        __delay_ms(1);
//        IMUWRITE2(cmdW,0x2E,0x00);//  en CTRL_REG5 pongo Interrupt is routed to INT2 pin;
//        IMUWRITE2(cmdW,0x2F,0x00);//  en CTRL_REG3 pongo interrupcion en bajo y push-pull
        MMA8471Activo();
        
        VectorRS[0]=0xFE;
        VectorRS[1]=0xc4;// 196 valores
        VectorRS[3]= ID;
        VectorRS[4]= orden;
        
        while(parar)
        {
        if (PORTAbits.RA8) //Espero que se levante RA8
        {
        IMUBREAD(0x00);// Leo registro F Status
        IMUREAD2(0x01, 192, VectorMMA);// Leo los 192 valores
        EnviarMMA();
        parar=0; //Fin de rutina FIFO

       MMA8471DesActivo();
        __delay_ms(1);
        IMUWRITE(0x09,0x00);//Sin FIFO
        __delay_ms(1);
        IMUWRITE(0x2D,0x00);//Sin Interrupciones
        __delay_ms(1);
        MMA8471Activo();
        __delay_ms(1);
        }
        }
        break;
        
        case LEER_ACEL_CONT:  //CASE 0x45 Leer aceleraciones en forma continua
        parar=2;
        MMA8471DesActivo();
        __delay_ms(1);
        IMUWRITE(0x09,0x00);//Fifo des habilitada
        __delay_ms(1);
        IMUWRITE(0x2D,0x01);//en CTRL_REG4 pongo INT_EN_DRDY=1
//        IMUWRITE2(cmdW,0x2E,0x00);//  en CTRL_REG5 pongo Interrupt is routed to INT2 pin;
//        IMUWRITE2(cmdW,0x2F,0x00);//  en CTRL_REG3 pongo interrupcion en bajo y push-pull
        __delay_ms(1);
        MMA8471Activo();
      __delay_ms(1);
        VectorRS[0]=0xFE;
        VectorRS[1]=23;// 23 valores
        VectorRS[2]= ID;
        VectorRS[3]= orden;
        Vcuenta=0;
        Trama=0;
        while(parar==2)
        {
        INT_SOURCE=IMUBREAD(0x0C);//leo INT_SOURCE
       
        if ((INT_SOURCE&0x01) == 0x01)// pregunto si hay interrupcion por INT_EN_DRDY
        {
            
   for(j=1;j<7;j++)
        {
        VectorRS[j+3+Vcuenta*6]=IMUBREAD(j);//
        __delay_us(5);
        }
          Vcuenta=Vcuenta+1;
   if (Vcuenta==3)
   {
       VectorRS[22]=Trama;
       EnviarTX();
        Vcuenta=0;
        Trama=Trama+1;
   } 
   }}
        
        break;
        
        case 0x46:                //parar de leer
        VectorRS[0]=0xFE;
        VectorRS[1]=0x4;// 4 valores
        VectorRS[2]=ID;// 201 valores
        VectorRS[3]= 0x46;
        EnviarTX();
        MMA8471DesActivo();
        IMUWRITE(0x09,0x00);//des habilito la Fifo
        __delay_ms(1);
        IMUWRITE(0x2D,0x00);//Sin Interrupciones
        __delay_ms(1);
        MMA8471Activo();
        __delay_ms(1);
        
        break;

        case ACEL_LOGGING_ON : //CASE 0x47 Almacenar en memoria la aceleracion
       switch(Bloque)
        {
                case 0x00:// Medicion 1
                    PageL=0;
                    PageH=1;
                    break;
                 
                case 0x01:// Medicion 2
                  PageL=0;
                  PageH=23;
                  break;
                 
                case 0x02:// Medicion 3
                 PageL=0;
                 PageH=45;
                 break;
                 
                case 0x03:// Medicion 4
                PageL=0;
                PageH=67;
                 break;
                 
                case 0x04:// Medicion 5
                 PageL=0;
                 PageH=89;
                 break;
                 
                case 0x05:// Medicion 6
                PageL=0;
                PageH=111;
                 break;
         }
                   
        parar=4;
        VectorRS[0]=0xFE;
        VectorRS[1]=4;// 3 valores
        VectorRS[2]= ID;
        VectorRS[3]= orden;
        EnviarTX();
         MMA8471DesActivo();
        __delay_ms(1);
        IMUWRITE(0x09,0x00);//Fifo des habilitada
        __delay_us(5);
        IMUWRITE(0x2D,0x01);//en CTRL_REG4 pongo INT_EN_DRDY=1
//        IMUWRITE2(cmdW,0x2E,0x00);//  en CTRL_REG5 pongo Interrupt is routed to INT2 pin;
//        IMUWRITE2(cmdW,0x2F,0x00);//  en CTRL_REG3 pongo interrupcion en bajo y push-pull
        __delay_us(5);
        MMA8471Activo();
        __delay_us(5);
        Vcuenta=0;
//        PageH=PageHi; // Paginas de inicios
//        PageL=PageLi;
        while(parar==4)
        {
        INT_SOURCE=IMUBREAD(0x0C);//leo INT_SOURCE
          
        if ((INT_SOURCE&0x01) == 0x01)// pregunto si hay interrupcion por INT_EN_DRDY
        {
           
        for(j=0;j<6;j++)
        {
        VectorMEM[j+Vcuenta*6]=IMUBREAD(j+1);//
         __delay_us(5);
        }
          Vcuenta=Vcuenta+1;
          
   if (Vcuenta==42)//Representa 252 bytes quedan 4 bytes libre
   {
       VectorMEM[252]=PageH;//almaceno el valor alto de la pagina
       VectorMEM[253]=PageL;
       EscribirPagina (PageH, PageL, 0, VectorMEM);// escribo la pagina con datos del acelerometro       
       PageL=PageL+1;
       if (PageL==0){PageH=PageH+1;}
       Vcuenta=0;
   } 
       //   if (PageH==1){parar=0;}//Por ahora termino con 80 segundos
   }}
        break;

        case ACEL_LOGGING_OFF:     //CASE 0x48 Parar de almacenar en memoria la aceleracion
            Bloque1=(Bloque+1)* 0x10;
        EscribirDato (0,Bloque1,0, PageL);//Prueba eb la pagima 0x0100XX
        __delay_ms(2);
        EscribirDato (0,Bloque1,1, PageH);//Prueba eb la pagima 0x0100XX
        
        VectorRS[0]=0xFE;
        VectorRS[1]=0x6;// 6 valores
        VectorRS[2]=ID;// 
        VectorRS[3]= orden;
        VectorRS[4]= PageH;// envio las paginas almacenadas
        VectorRS[5]= PageL;
        EnviarTX();
        break;
        
        case Leer_ACEL  : //CASE 0x49 Leer datos Almacenados  en memoria 
        
        LeerPagina (DireH, DireL,0,VectorMEM);//Por ahora leo la pagina cero
        VectorRS[0]=0xFE;
        VectorRS[1]=0x04;// 4 valores
        VectorRS[2]=ID;// 
        VectorRS[3]= orden;
        while (U1STAbits.TRMT==0);
        U1TXREG=VectorRS[0];
        while (U1STAbits.TRMT==0);
        U1TXREG= VectorRS[1];
        while (U1STAbits.TRMT==0);
        U1TXREG= VectorRS[2];
        while (U1STAbits.TRMT ==0);
        U1TXREG= VectorRS[3];
        for (j=0;j<256;j++)
        {
        while (U1STAbits.TRMT==0);
        U1TXREG= VectorMEM[j];
        }

        break;
        
        case WhoamI:     //CASE 0x4D Quien soy yo
        VectorRS[0]=0xFE;
        VectorRS[1]=0x5;// 5 valores
        VectorRS[2]=ID;// Quien soy YO
        VectorRS[3]= orden;
        VectorRS[4]=IMUBREAD(0x0D);// Leo todos  el registro 0x0D
        EnviarTX();
        //EnviarTX2();  //Prueba
        break;

        case LeerMemD:                //case 0x50 leer un dato memoria serial
//             
        VectorRS[0]=0xFE;
        VectorRS[1]=0x05;// 5 valores
        VectorRS[2]=ID;//envio del ID
        VectorRS[3]= orden;
        VectorRS[4]= LeerDato (DireH,Dire,DireL);//Prueba eb la pagima 0x0100XX
        EnviarTX();
        break;
        
        case EscMemD:                //case 0x51 escribir un dato memoria serial
        EscribirDato (DireH,Dire,DireL, Valor);//Prueba eb la pagima 0x0100XX
        VectorRS[0]=0xFE;
        VectorRS[1]=0x04;// 4 valores
        VectorRS[2]=ID;//envio del ID
        VectorRS[3]= orden;
        EnviarTX();
         __delay_ms(25);// espero que se grabe el dato
        break;
        
        case LeerMemID:                //case 0x52 leer ID memoria serial
        //EscribirDato (DireH,Dire,DireL, Valor);//Prueba eb la pagima 0x0100XX
        ReadIdentification (&OUT_X,&OUT_Y,&OUT_Z);
        VectorRS[0]=0xFE;
        VectorRS[1]=0x07;// 3 valores
        VectorRS[2]=ID;//envio del ID
        VectorRS[3]= orden;
        VectorRS[4]= OUT_X;
        VectorRS[5]= OUT_Y;
        VectorRS[6]= OUT_Z;
        EnviarTX();
        break;
        
         case ResetMem:                //case 0x53 reset memoria serial
        //EscribirDato (DireH,Dire,DireL, Valor);//Prueba eb la pagima 0x0100XX
        ChipErase();
        VectorRS[0]=0xFE;
        VectorRS[1]=0x04;// 3 valores
        VectorRS[2]= ID;//
        VectorRS[3]= orden;
        EnviarTX();
        break;
        
        case LeerStatusMem:                //case 0x54 Leer Status  memoria serial
        LeerStatus1(&Dato);
        VectorRS[0]=0xFE;
        VectorRS[1]=0x05;// 5 valores
        VectorRS[2]=ID;// 
        VectorRS[3]= orden;
         VectorRS[4]= Dato;
        // VectorRS[3]=LeerStatus();
        EnviarTX();
        break;
        
        case EscStatusMem:                //case 0x55 Esccribir Status  memoria serial
        EscribirStatus (Valor);
        VectorRS[0]=0xFE;
        VectorRS[1]=0x04;// 4 valores
        VectorRS[2]=ID;// 3 valores
        VectorRS[3]= orden;
        EnviarTX();
        break;
        
        case UnlockProMem:  //case 0x56 Unlock  memoria serial
        UnlockMem ();  //Global Block Protection Unlock
        VectorRS[0]=0xFE;
        VectorRS[1]=0x04;// 3 valores
        VectorRS[2]=ID;// 
        VectorRS[3]= orden;
        EnviarTX();
        break;
        
        case LeerConfMem:                //case 0x57 Leer Reg Configuracion memoria serial
        LeerConf1(&Dato);
        VectorRS[0]=0xFE;
        VectorRS[1]=0x05;// 5 valores
        VectorRS[2]=ID;// 
        VectorRS[3]= orden;
         VectorRS[4]= Dato;
        // VectorRS[3]=LeerStatus();
        EnviarTX();
        break;
        
        case LeerPageMem:                //case 0x58 Leer pagina memoria serial
        LeerPagina (DireH, DireL,0,VectorMEM);//Por ahora leo la pagina cero
        VectorRS[0]=0xFE;
        VectorRS[1]=0x04;// 3 valores
        VectorRS[2]=ID;
        VectorRS[3]= orden;
        while (U1STAbits.TRMT==0);
        U1TXREG=VectorRS[0];
        while (U1STAbits.TRMT==0);
        U1TXREG= VectorRS[1];
        while (U1STAbits.TRMT==0);
        U1TXREG= VectorRS[2];
        while (U1STAbits.TRMT==0);
        U1TXREG= VectorRS[3];
        for (j=0;j<256;j++)
        {
        while (U1STAbits.TRMT==0);
        U1TXREG= VectorMEM[j];
        }
        break;
        
        case EscPageMem: //case 0x59 Leer pagina memoria serial
        for(j=0;j<256;j++)
        {
        VectorMEM[j]=Valor+j;
        }
        EscribirPagina (0, Dire, 0, VectorMEM);//Por ahora escribo la pagina cero
        VectorRS[0]=0xFE;
        VectorRS[1]=0x03;// 4 valores
        VectorRS[2]=ID;//
        VectorRS[3]= orden;
        EnviarTX();
        break;
        
        case Reset:     //case 0xA5 reset al acelerometro
        MMA8471Reset();
        VectorRS[0]=0xFE;
        VectorRS[1]=0x04;// 3 valores
        VectorRS[2]=ID;// 
        VectorRS[3]= orden;
        EnviarTX();
        break;
        
/// Falta modificar esta parte de codigo, se debe incorporar el envio del ID        
        
        case MapanRF:     //case 0x83 leo mapa de registro del nRF
        VectorRS[0]=0xFE;
        VectorRS[1]=0x28;// 40 valores
        VectorRS[2]= orden;
        //Agregar lectura SPI3
        
        CS_RF=0; // Bajo el CS del nRF
        //Para leer un registro R_REGISTER=000A AAAA
                    
        for(i=0;i<10;i++){
        CS_RF=0;
        WByteSPI3(i);//Leo distintos registros direccion 0 a 9   
        VectorRS[i+3] = RByteSPI3();	
        CS_RF=1; // Levanto el CS del nRF
        }
        CS_RF=0;
        WByteSPI3(0x0A);//Leo  registro RX_ADDR_P0    
        VectorRS[13] = RByteSPI3();	
        VectorRS[14] = RByteSPI3();//
        VectorRS[15] = RByteSPI3();	
        VectorRS[16] = RByteSPI3();//	
        VectorRS[17] = RByteSPI3();
        CS_RF=1; // Levanto el CS del nRF 
        __delay_us(10);
        CS_RF=0;
        WByteSPI3(0x0B);// Leo  registro RX_ADDR_P1  
        VectorRS[18] = RByteSPI3();	
        VectorRS[19] = RByteSPI3();//
        VectorRS[20] = RByteSPI3();	
        VectorRS[21] = RByteSPI3();//	
        VectorRS[22] = RByteSPI3();
        CS_RF=1; // Levanto el CS del nRF 
        __delay_us(10);
        CS_RF=0;
        WByteSPI3(0x0C);//Leo  registro RX_ADDR_P2    
        VectorRS[23] = RByteSPI3();	
        CS_RF=1; // Levanto el CS del nRF
        __delay_us(10);
        CS_RF=0;
        WByteSPI3(0x0D);//Leo  registro RX_ADDR_P3 
        VectorRS[24] = RByteSPI3();	
        CS_RF=1; // Levanto el CS del nRF
        __delay_us(10);
        CS_RF=0;
        WByteSPI3(0x0E);//Leo  registro RX_ADDR_P4   
        VectorRS[25] = RByteSPI3();	
        CS_RF=1; // Levanto el CS del nRF
        __delay_us(10);
        CS_RF=0;
        WByteSPI3(0x0F);//Leo  registro RX_ADDR_P5   
        VectorRS[26] = RByteSPI3();	
        CS_RF=1; // Levanto el CS del nRF
        __delay_us(10);
        CS_RF=0;
        WByteSPI3(0x10);//Leo  registro TX_ADDR    
        VectorRS[27] = RByteSPI3();	
        VectorRS[28] = RByteSPI3();//
        VectorRS[29] = RByteSPI3();	
        VectorRS[30] = RByteSPI3();//	
        VectorRS[31] = RByteSPI3();
        CS_RF=1; // Levanto el CS del nRF 
                    
        for(i=0x11;i<0x18;i++){
        CS_RF=0;
        WByteSPI3(i);//Leo distintos registros direccion 17 a 23   
        VectorRS[i+15] = RByteSPI3();	
        CS_RF=1; // Levanto el CS del nRF
        }
        __delay_us(10); 
        CS_RF=0;
        WByteSPI3(0x1c);//Leo registro DYNPD 
        VectorRS[39] = RByteSPI3();	
        CS_RF=1; // Levanto el CS del nRF
        __delay_us(10); 
        CS_RF=0;
        WByteSPI3(0x1d);//Leo registro Feature 
        VectorRS[40] = RByteSPI3();	
        CS_RF=1; // Levanto el CS del nRF
        EnviarTX();
        break;
        
        
        case WReg1nRF:
        VectorRS[0]=0xFE;
        VectorRS[1]=0x3;// 3 valores
        VectorRS[2]= orden;
        CS_RF=0; // Bajo el CS del nRF
        Dire=Dire|0x20;
        WByteSPI3(Dire);//Direccion
        WByteSPI3(Valor);//Dato
        __delay_us(1);	
        CS_RF=1; // Levanto el CS del nRF
        EnviarTX();
        break;
        
        case wReg5nRF:   //escribir cinco registro nRF. 0x86
        VectorRS[0]=0xFE;
        VectorRS[1]=0x3;// 35 valores
        VectorRS[2]= orden;            
                    CS_RF=0; // Bajo el CS del nRF
                    Vector[0]=Vector[0]|0x20;
                    WByteSPI3(Vector[0]);//Direccion
                    WByteSPI3(Vector[1]);//Dato
                    WByteSPI3(Vector[2]);//Dato
                    WByteSPI3(Vector[3]);//Dato
                    WByteSPI3(Vector[4]);//Dato
                    WByteSPI3(Vector[5]);//Dato
                    __delay_us(1);	
                    CS_RF=1; // Levanto el CS del nRFLeerFIFOnRF
                    EnviarTX();           
        break;
        
        case LeerFIFOnRF:     //case 0x87 leo FIFO de registro del nRF
        VectorRS[0]=0xFE;
        VectorRS[1]=0x23;// 3 valores
        VectorRS[2]= orden;
              
        CS_RF=0; // Bajo el CS del nRF
        //Para leer un registro R_REGISTER=000A AAAA
        WByteSPI3(0x61);//
        for(i=3;i<35;i++){
        VectorRS[i] = RByteSPI3();	
        }
        CS_RF=1; // Levanto el CS del nRF
        EnviarTX();
        break;
        
        case EscFIFOnRFACK:   //escribir FIFO con ACK nRF. 0x88
        CS_RF=0; // Bajo el CS del nRF
        WByteSPI3(0xA0);//
        for(i=0;i<Nbytes-3;i++){
        WByteSPI3(VectorRS[i]);	
        }
        __delay_us(1);
        CS_RF=1; // Levanto el CS del nRF
        __delay_us(1);
        CE_RF=1;
        __delay_us(20);
        CE_RF=0;
        VectorRS[0]=0xFE;
        VectorRS[1]=0x3;// 3 valores
        VectorRS[2]= orden; 
        EnviarTX();           
        break;
        
        case RXnRF:   //recepcion al RF. 0x89
        VectorRS[0]=0xFE;
        VectorRS[1]=0x3;// 3 valores
        VectorRS[2]= orden; 
        CS_RF=0; // Bajo el CS del nRF 
        WByteSPI3(0x00);//Leo  registro CONFIG    
        datt = RByteSPI3();	
        datt=datt|0x3;//POWER UP y RX/TX en RX en uno
        CS_RF=1; // Levanto el CS del nRF
        __delay_us(10);
        CS_RF=0; // Bajo el CS del nRF 
        WByteSPI3(0x20);//Direcciono registro CONFIG
        WByteSPI3(datt);
        __delay_us(10);	
        CS_RF=1; // Levanto el CS del nRF
        CE_RF=1;//Enciendo el equipo de RADIO
        EnviarTX();           
        break;
        
        case TXnRF:   //transmicion al nRF. 0x8A
        VectorRS[0]=0xFE;
        VectorRS[1]=0x3;// 3 valores
        VectorRS[2]= orden; 
        CS_RF=0; // Bajo el CS del nRF 
        WByteSPI3(0x00);//Leo  registro CONFIG    
        datt = RByteSPI3();	
        datt=datt|0x02;//POWER UP en 1 
        datt=datt&0xFE;// RX/TX en TX en cero
        CS_RF=1; // Levanto el CS del nRF
        __delay_us(10);
        CS_RF=0; // Bajo el CS del nRF 
        WByteSPI3(0x20);//Direcciono registro CONFIG
        WByteSPI3(datt);
        __delay_us(1);	
        CS_RF=1; // Levanto el CS del nRF
        CE_RF=0;//Enciendo el equipo de RADIO
        EnviarTX();           
        break;
        
        case SBynRF:   //StandBy al RF. 0x8B
        VectorRS[0]=0xFE;
        VectorRS[1]=0x3;// 3 valores
        VectorRS[2]= orden; 
        CS_RF=0; // Bajo el CS del nRF 
        WByteSPI3(0x00);//Leo  registro CONFIG    
        datt=datt&0xFC;//POWER UP y RX/TX en RX en cero
        CS_RF=1; // Levanto el CS del nRF
        __delay_us(10);
        CS_RF=0; // Bajo el CS del nRF 
        WByteSPI3(0x20);//Direcciono registro CONFIG
        WByteSPI3(datt);
        __delay_us(10);	
        CS_RF=1; // Levanto el CS del nRF
        __delay_us(1);
        CE_RF=0;//Enciendo el equipo de RADIO
        EnviarTX();           
        break;
        
        case EscFIFOnRFNACK:   //escribir FIFO sin ACK nRF. 0x8D
                    
        CS_RF=0; // Bajo el CS del nRF
        WByteSPI3(0xB0);//
        for(i=0;i<Nbytes-3;i++){
        WByteSPI3(VectorRS[i]);	
        }
        __delay_us(1);	
        CS_RF=1; // Levanto el CS del nRF
        __delay_us(1);	
        CE_RF=1;
        __delay_us(20);
        CE_RF=0;
        VectorRS[0]=0xFE;
        VectorRS[1]=0x3;// 3 valores
        VectorRS[2]= orden;
        EnviarTX();           
        break;
        
        case CFIFORXnRF:   //Limpiar FIFO RX nRF. 0x8E
        VectorRS[0]=0xFE;
        VectorRS[1]=0x3;// 3 valores
        VectorRS[2]= orden;            
        CS_RF=0; // Bajo el CS del nRF
        WByteSPI3(0xE2);//
        __delay_us(1);	
        CS_RF=1; // Levanto el CS del nRF
        EnviarTX();           
        break;
        
        case CFIFOTXnRF:   //Limpiar FIFO RX nRF. 0x8F
        VectorRS[0]=0xFE;
        VectorRS[1]=0x3;// 3 valores
        VectorRS[2]= orden;            
        CS_RF=0; // Bajo el CS del nRF
        WByteSPI3(0xE1);//
        __delay_us(1);	
        CS_RF=1; // Levanto el CS del nRF
        EnviarTX();           
        break;
        
        case PruINT:        //case 0xAB Prueba de lectura de las aceleraciones con Interrupciones:
        MMA8471DesActivo();
        __delay_ms(10);
        IMUWRITE(0x2D,0x01);//en CTRL_REG4 pongo INT_EN_DRDY=1
        IMUWRITE(0x2E,0x01);//en CTRL_REG5 pongo INT_CFG_DRDY=1
        MMA8471Activo();//Ponemos al MMA en modo activo
        IntInit(); //Habilito las interrupciones por INT1
        break;
        
        default: 
        Nop();
        break;
        }               
        }
 
        if(Kbhit2==1) //Pregunto si llega algo por USART2
          {
             Kbhit2=0;
        switch(orden)
        {
            
        case 0x03: // Medicion por tiempo    
         //borramos la memoria
        //ChipErase();
         switch(Bloque)
        {
                case 0x00:// Medicion 1
                    PageL=0;
                    PageH=1;
                    break;
                 
                case 0x01:// Medicion 2
                  PageL=0;
                  PageH=23;
                  break;
                 
                case 0x02:// Medicion 3
                 PageL=0;
                 PageH=45;
                 break;
                 
                case 0x03:// Medicion 4
                PageL=0;
                PageH=67;
                 break;
                 
                case 0x04:// Medicion 5
                 PageL=0;
                 PageH=89;
                 break;
                 
                case 0x05:// Medicion 6
                PageL=0;
                PageH=111;
                 break;
         }    
            
        muestras_P = char2long.vlong;
         MMA8471DesActivo();
                    __delay_ms(1);
                    IMUWRITE(0x09, 0x00); //Fifo des habilitada
                    __delay_us(5);
                    IMUWRITE(0x2D, 0x01); //en CTRL_REG4 pongo INT_EN_DRDY=1
                    __delay_us(5);
                    MMA8471Activo();
                    __delay_us(5);
                    cuenta_M=0;
                    Vcuenta=0;
//                    PageL=0;
//                    PageH=0;
                    while((cuenta_M < muestras_P)&& parar!=0)//almacenamos las aceleraciones muestras_P veces
                    {
                        INT_SOURCE = IMUBREAD(0x0C); //leo INT_SOURCE
                        if ((INT_SOURCE & 0x01) == 0x01)// pregunto si hay interrupcion por INT_EN_DRDY
                        {
                            for (j = 0; j < 6; j++) {
                                VectorMEM[j + Vcuenta * 6] = IMUBREAD(j + 1); //
                                __delay_us(5);
                            }
                            Vcuenta++;
                            cuenta_M++;
                            if (Vcuenta == 42)//Representa 252 bytes quedan 4 bytes libre
                            {
                                VectorMEM[252] = PageH; //almaceno el valor alto de la pagina
                                VectorMEM[253] = PageL;
                                EscribirPagina(PageH, PageL, 0, VectorMEM); // escribo la pagina con datos del acelerometro       
                                PageL = PageL + 1;
                                if (PageL == 0) {
                                    PageH = PageH + 1;
                                }
                                Vcuenta = 0;
                            }
                        }
                    }
       break;
        
            case 0x04:  // Trigger
            //borramos la memoria
            //ChipErase();
            switch(Bloque)
        {
                case 0x00:// Medicion 1
                    PageL=0;
                    PageH=1;
                    BloqueBK=PageH;
                    break;
                 
                case 0x01:// Medicion 2
                  PageL=0;
                  PageH=23;
                  BloqueBK =PageH;
                  break;
                 
                case 0x02:// Medicion 3
                 PageL=0;
                 PageH=45;
                 BloqueBK =PageH;
                 break;
                 
                case 0x03:// Medicion 4
                PageL=0;
                PageH=67;
                BloqueBK=PageH;
                 break;
                 
                case 0x04:// Medicion 5
                 PageL=0;
                 PageH=89;
                 BloqueBK=PageH;
                 break;
                 
                case 0x05:// Medicion 6
                PageL=0;
                PageH=111;
                BloqueBK=PageH;
                 break;
         }
            muestras_P = char2long.vlong;
            //conf_recepcion_slave1 (); // ver que hace por ahora no se usa   
                     MMA8471DesActivo();
                    __delay_ms(1);
                    IMUWRITE(0x09, 0x00); //Fifo des habilitada
                    __delay_us(5);
                    IMUWRITE(0x2D, 0x01); //en CTRL_REG4 pongo INT_EN_DRDY=1
                    __delay_us(5);
                    MMA8471Activo();
                    __delay_us(5);
                    if (Master ==1 )//yo soy el maestro 
                    {
                        INT_SOURCE = IMUBREAD(0x0C); //leo INT_SOURCE
                        while ((INT_SOURCE & 0x01) == 0x01) {// pregunto si hay interrupcion por INT_EN_DRDY
                            INT_SOURCE = IMUBREAD(0x0C);
                        } //leo INT_SOURCE
                        for (j = 0; j < 6; j++) {
                            cachito2[j] = IMUBREAD(j + 1);
                            __delay_us(5);
                        }
                        cachito2signed[0] = cachito2[0]; //signed MSB x
                        cachito2signed [1] = cachito2[2]; //signed MSB y
                        cachito2signed[2] = cachito2[4]; //signed MSB Z
                   }
                    parar = 2;
                    Vcuenta = 0;
                    cancelar=1;
                    //almacenamiento ciclico de mediciones
                    while (parar != 0 && cancelar!=0) {
                        INT_SOURCE = IMUBREAD(0x0C); //leo INT_SOURCE
                        if ((INT_SOURCE & 0x01) == 0x01)// pregunto si hay interrupcion por INT_EN_DRDY
                        {
                            for (j = 0; j < 6; j++) {
                                __delay_us(5);
                                circular[j + Vcuenta * 6] = IMUBREAD(j + 1); //guardo la cola en la variable circular                               
                         }
                          if (Master ==1 ) {
                                //combinamos los datos medidos en signed int y lo comparamos con el almacenado
                                circularsigned[0] = circular[ Vcuenta * 6]; //MSB x estado actual
                                circularsigned[1] = circular[2 + Vcuenta * 6]; //MSB y estado actual
                                circularsigned[2] = circular[4 + Vcuenta * 6]; //MSB z estado actual
                                
                             if (((circularsigned[0] > cachito2signed[0] + Trigger) || (circularsigned[0] < cachito2signed[0] - Trigger)) || ((circularsigned[1] > cachito2signed[1] + Trigger) || (circularsigned[1] < cachito2signed[1] - Trigger)) || ((circularsigned[2] > cachito2signed[2] + Trigger) || (circularsigned[2] < cachito2signed[2] - Trigger))) {
                                    cancelar = 0;
                                    VectorRS[0] = 0xFD;
                                    VectorRS[1] = 0x04; // comando para que se detengan los demas sensores
                                    VectorRS[2] = 0x00;
                                    VectorRS[3] = 0x01;
                                    EnviarTX2();
                                  }
                                
                            }
                            Vcuenta = Vcuenta + 1;
                            if (Vcuenta > muestras_C * 42) {//reinicio  Vcuenta
                                Vcuenta = 0;
                             }
                        } }
                     indice_C = Vcuenta; //guardamos donde guardamos el ultimo dato en la memoria circular
                    Vcuenta = 0;
                    PageL = muestras_C; //empezamos a guardar nuevas mediciones dejando espacio para la cola
                   // PageH = 0;
                    cuenta_M = 0;
                    //UnlockMem();
                    while ((cuenta_M < muestras_P) && parar !=0)  //almacenamos las aceleraciones muestras_P veces
                    {
                        INT_SOURCE = IMUBREAD(0x0C); //leo INT_SOURCE
                        if ((INT_SOURCE & 0x01) == 0x01)// pregunto si hay interrupcion por INT_EN_DRDY
                        {
                            for (j = 0; j < 6; j++) {
                                VectorMEM[j + Vcuenta * 6] = IMUBREAD(j + 1); //
                                __delay_us(5);
                            }
                            Vcuenta = Vcuenta + 1;
                            cuenta_M++;
                            if (Vcuenta == 42)//Representa 252 bytes quedan 4 bytes libre
                            {
                                VectorMEM[252] = PageH; //almaceno el valor alto de la pagina
                                VectorMEM[253] = PageL;
                                EscribirPagina(PageH, PageL, 0, VectorMEM); // escribo la pagina con datos del acelerometro       
                                PageL = PageL + 1;
                                if (PageL == 0) {
                                    PageH = PageH + 1;
                                }
                                Vcuenta = 0;
                            }
                        }
                    }
                    UltimaL=PageL;
                    UltimaH=PageH;
                    //escribimos la memoria ciclica en la mem flash
                    PageH = BloqueBK;
                    PageL = 0;
                    g = 0;
                    V = indice_C;
                    //UnlockMem();
                    for (S = 0; S <= (muestras_C * 42); S++) {
                        memcpy(VectorMEM + g * 6, circular + V * 6, 6);
                        g++;
                        V++;
                        //completamos una pagina y la grabamos en memoria
                        if (g == 42) {
                            g = 0;
                            VectorMEM[252] = PageH; //almaceno el valor alto de la pagina
                            VectorMEM[253] = PageL;
                            EscribirPagina(PageH, PageL, 0, VectorMEM); // escribo la pagina con datos del acelerometro 
                            PageL = PageL + 1;
                            if (PageL == 0) {//cuando se desborda PageL suma 1 a PageH
                                PageH = PageH + 1;
                            }
                        }
                        if (V == (muestras_C * 42)) {
                            V = 0;
                        }
                    }
                    PageL=UltimaL;
                    PageH=UltimaH;
                    
                        break;
        
         case ArchSD:// Generar archivo SD
                    VectorRS[0] = 0xFE;
                    VectorRS[1] = 0x5; // 4 valores
                    VectorRS[2] = ID;
                    VectorRS[3] = orden;
                    // Register the GetTimestamp function as the timestamp source for the library.
                    FILEIO_RegisterTimestampGet(GetTimestamp);
                    ENTERO = VectorMEM[6 * j]*256 + VectorMEM[6 * j + 1];
                    get_int_hex(ENTERO, Name); //paso de int a ASCII en Hexa
                    if (FILEIO_Initialize() == -1) {
                        VectorRS[4] = 0x01; //error  de inicializacion de archivo
                        EnviarTX2();
                        break; //finaliza proceso de grabar archivo en SD e indica error al soft
                    }
                    if (FILEIO_MediaDetect(&gSdDrive, &sdCardMediaParameters) == false) {
                        VectorRS[4] = 0x02; //SD no se detecta
                        EnviarTX2();
                        break; //finaliza proceso de grabar archivo en SD e indica error al soft
                    }
                    if (FILEIO_DriveMount('A', &gSdDrive, &sdCardMediaParameters) != FILEIO_ERROR_NONE)
                        //Mount the SD card as a drive with drive letter 'A'    
                    {
                        VectorRS[4] = 0x03; //SD no se monta
                        EnviarTX2();
                        break; //finaliza proceso de grabar archivo en SD e indica error al soft
                    }


                    // Open a file called  and store the file data in the FILEIO_OBJECT "file."
                    // This file is being opened with the CREATE flag, which will allow the library to create the file if it doesn't already exist, the
                    // TRUNCATE flag, which will truncate the file to a 0-length file if it already exists, the WRITE flag, which will allow us to
                    // write to the file, and the READ flag, which will allow us to read from it.

                    if (Valor == 0)//  comienzo pagina 0
                    {
                        PageH = 1;
                        if (FILEIO_Open(&file, "LECT1.CSV", FILEIO_OPEN_WRITE | FILEIO_OPEN_APPEND | FILEIO_OPEN_CREATE) == FILEIO_RESULT_FAILURE) {
                            VectorRS[4] = 0x04; //El archivo no se pudo crear
                            EnviarTX2();
                            break; //finaliza proceso de grabar archivo en SD e indica error al soft
                        }
                    }

                    if (Valor == 1) {
                        PageH = 23; //  comienzo pagina
                        if (FILEIO_Open(&file, "LECT2.CSV", FILEIO_OPEN_WRITE | FILEIO_OPEN_APPEND | FILEIO_OPEN_CREATE) == FILEIO_RESULT_FAILURE) {
                            VectorRS[4] = 0x04; //El archivo no se pudo crear
                            EnviarTX2();
                            break; //finaliza proceso de grabar archivo en SD e indica error al soft
                        }
                    }

                    if (Valor == 2)//  comienzo pagina 0x2000
                    {
                        PageH = 45;
                        if (FILEIO_Open(&file, "LECT3.CSV", FILEIO_OPEN_WRITE | FILEIO_OPEN_APPEND | FILEIO_OPEN_CREATE) == FILEIO_RESULT_FAILURE) {
                            VectorRS[4] = 0x04; //El archivo no se pudo crear
                            EnviarTX2();
                            break; //finaliza proceso de grabar archivo en SD e indica error al soft
                        }
                    }

                    if (Valor == 3)//  comienzo pagina 0x3000
                    {
                        PageH = 67;
                        if (FILEIO_Open(&file, "LECT4.CSV", FILEIO_OPEN_WRITE | FILEIO_OPEN_APPEND | FILEIO_OPEN_CREATE) == FILEIO_RESULT_FAILURE) {
                            VectorRS[4] = 0x04; //El archivo no se pudo crear
                            EnviarTX2();
                            break; //finaliza proceso de grabar archivo en SD e indica error al soft
                        }
                    }

                    if (Valor == 4)//  comienzo pagina 0x4000
                    {
                        PageH = 89; //  
                        if (FILEIO_Open(&file, "LECT5.CSV", FILEIO_OPEN_WRITE | FILEIO_OPEN_APPEND | FILEIO_OPEN_CREATE) == FILEIO_RESULT_FAILURE) {
                            VectorRS[4] = 0x04; //El archivo no se pudo crear
                            EnviarTX2();
                            break; //finaliza proceso de grabar archivo en SD e indica error al soft
                        }
                    }

                    if (Valor == 5)//  comienzo pagina 0x5000
                    {
                        PageH = 111; //  
                        if (FILEIO_Open(&file, "LECT6.CSV", FILEIO_OPEN_WRITE | FILEIO_OPEN_APPEND | FILEIO_OPEN_CREATE) == FILEIO_RESULT_FAILURE) {
                            VectorRS[4] = 0x04; //El archivo no se pudo crear
                            EnviarTX2();
                            break; //finaliza proceso de grabar archivo en SD e indica error al soft
                        }
                    }
                    VectorRS[4] = 0x00; //El archivo se creo con exito
                    EnviarTX2();
    IFS0bits.U1RXIF = 0;// Limpio la bandera de la interrupción
	IEC0bits.U1RXIE = 0;//Deshabilito la interrupción por recepción RX
                    

                   
                    DireL = 0;
                    for (Cuenta = 0; Cuenta < 0x1600; Cuenta++)// leer 1,6Mbytes
                        /*
                        8 Mbytes de la memoria serial representa 0x8000 paginas de 256 byts cada uno 
                         La memoria tiene 128 sectores de 64 kbytes
                        ( 8 lecturas de 1 Mbytes  es 0x1000 paginass (decimal 4096)
                         * quedando cada medicion de 0x1000 pagina, que representa mas de 3,5 minutos
                         */ {
                        DireH = DireH +(Cuenta >> 8);
                        DireL = Cuenta;
                        LeerPagina(DireH, DireL, 0, VectorMEM); //
                        for (j = 0; j < 42; j++)// Paso a ASCII los 42 valores
                        {
                            ENTERO = VectorMEM[6 * j]*256 + VectorMEM[6 * j + 1];
                            get_int_hex(ENTERO, Str1); //paso de int a ASCII en Hexa
                            strcat(Str, Str1);
                            ENTERO = VectorMEM[6 * j + 2]*256 + VectorMEM[6 * j + 3];
                            get_int_hex(ENTERO, Str1); //paso de int a ASCII en Hexa
                            strcat(Str, Str1);
                            ENTERO = VectorMEM[6 * j + 4]*256 + VectorMEM[6 * j + 5];
                            get_int_hex(ENTERO, Str1); //paso de int a ASCII en Hexa
                            strcat(Str, Str1);
                            strcat(Str, "\r\n");
                            FILEIO_Write(Str, 1, 17, &file); //escribo un renglon en el archivo
                            Str[0] = 0; //Forzamos en primer caracte en nulo
                        }
                        //        ENTERO=VectorMEM[252]*256+VectorMEM[253];    
                        //        get_int_hex( ENTERO,Str1 );//paso de int a ASCII en Hexa
                        //        strcat(Str, Str1 );
                        //        strcat(Str,"\r\n");
                        //        FILEIO_Write (Str, 1, 6, &file)  ;  //escribo un renglon en el archivo
                        //        Str[0]=0; //Forzamos en primer caracte en nulo
                        //       __delay_ms(10);//prueba
                    }

                    FILEIO_Close(&file);
        IFS0bits.U1RXIF = 0;// Limpio la bandera de la interrupción
        IEC0bits.U1RXIE = 1;//Habilito la interrupción por recepción RX
      
        break;    
        
        case 0x05://Comando borrar bloques de memoria
                
                switch(Bloque)
                {   case 0x00:// Medicion 1
                    for (j=1;j<23;j++)
                    { ClearBloq( j);}
                    __delay_ms(100);
                    VectorRS[0]=0xFE;
                    VectorRS[1]=0x05;// 5 valores
                    VectorRS[2]= ID;
                    VectorRS[3]= orden;
                    VectorRS[4]= Bloque;
                    EnviarTX2();
                    break;
                 
                case 0x01:// Medicion 2
                  for (j=23;j<45;j++)
                 { ClearBloq( j);}          
                  __delay_ms(100);
                  VectorRS[0]=0xFE;
                    VectorRS[1]=0x05;// 5 valores
                    VectorRS[2]= ID;
                    VectorRS[3]= orden;
                    VectorRS[4]= Bloque;
                    EnviarTX2();
                  break;
                 
                case 0x02:// Medicion 3
                 for (j=45;j<67;j++)
                 { ClearBloq( j);}       
                  __delay_ms(100);
                  VectorRS[0]=0xFE;
                    VectorRS[1]=0x05;// 5 valores
                    VectorRS[2]= ID;
                    VectorRS[3]= orden;
                    VectorRS[4]= Bloque;
                    EnviarTX2();
                 break;
                 
                case 0x03:// Medicion 4
                 for (j=68;j<89;j++)
                 { ClearBloq( j);}          
                  __delay_ms(100);
                  VectorRS[0]=0xFE;
                    VectorRS[1]=0x05;// 5 valores
                    VectorRS[2]= ID;
                    VectorRS[3]= orden;
                    VectorRS[4]= Bloque;
                    EnviarTX2();
                 break;
                 
                case 0x04:// Medicion 5
                 for (j=89;j<111;j++)
                 { ClearBloq( j);}      
                  __delay_ms(100);
                  VectorRS[0]=0xFE;
                    VectorRS[1]=0x05;// 5 valores
                    VectorRS[2]= ID;
                    VectorRS[3]= orden;
                    VectorRS[4]= Bloque;
                    EnviarTX2();
                 break;
                 
                case 0x05:// Medicion 6
                 for (j=111;j<133;j++)
                 { ClearBloq( j);}          
                  __delay_ms(100);
                  VectorRS[0]=0xFE;
                    VectorRS[1]=0x05;// 5 valores
                    VectorRS[2]= ID;
                    VectorRS[3]= orden;
                    VectorRS[4]= Bloque;
                    EnviarTX2();
                 break;
                    
                case 0xFF:// borrar toda la memoria
                for (j=1;j<133;j++)
                 { ClearBloq( j);}          
                  __delay_ms(100);  
                 __delay_ms(100);
                 VectorRS[0]=0xFE;
                    VectorRS[1]=0x05;// 5 valores
                    VectorRS[2]= ID;
                    VectorRS[3]= orden;
                    VectorRS[4]= Bloque;
                    EnviarTX2();
                break;
            
                }
                break;
         
        case 0x07:// seleccionar bloqueFalta ID
        VectorRS[0] = 0xFE;
        VectorRS[1] = 0x05;
        VectorRS[2]= ID;
        VectorRS[3]= orden;
        VectorRS[4]= Bloque;
        EnviarTX2();
        break;        
        
        case 0x08://Comando borrar Sectores de memoria
               switch(Sector)
               {
                   
                 case 0x00:// Medicion 1
                   SectorErase(0,0x10,0 );
                    __delay_ms(100);
                    VectorRS[0]=0xFE;
                    VectorRS[1]=0x05;// 5 valores
                    VectorRS[2]= ID;
                    VectorRS[3]= orden;
                    VectorRS[4]= Sector;
                    EnviarTX2();
                    break;
                 
                case 0x01:// Medicion 2
                    SectorErase(0,0x20,0 );
                    __delay_ms(100);
                    VectorRS[0]=0xFE;
                    VectorRS[1]=0x05;// 5 valores
                    VectorRS[2]= ID;
                    VectorRS[3]= orden;
                    VectorRS[4]= Sector;
                    EnviarTX2();
                    break;
                                
                case 0x02:// Medicion 3
                    SectorErase(0,0x30,0 );
                    __delay_ms(100);
                    VectorRS[0]=0xFE;
                    VectorRS[1]=0x05;// 5 valores
                    VectorRS[2]= ID;
                    VectorRS[3]= orden;
                    VectorRS[4]= Sector;
                    EnviarTX2();
                    break;
               
                 
                case 0x03:// Medicion 4
                    SectorErase(0,0x40,0 );
                    __delay_ms(100);
                    VectorRS[0]=0xFE;
                    VectorRS[1]=0x05;// 5 valores
                    VectorRS[2]= ID;
                    VectorRS[3]= orden;
                    VectorRS[4]= Sector;
                    EnviarTX2();
                    break;
                                
                case 0x04:// Medicion 5
                    SectorErase(0,0x50,0 );
                    __delay_ms(100);
                    VectorRS[0]=0xFE;
                    VectorRS[1]=0x05;// 5 valores
                    VectorRS[2]= ID;
                    VectorRS[3]= orden;
                    VectorRS[4]= Sector;
                    EnviarTX2();
                    break;
               
                 
                case 0x05:// Medicion 6
                    SectorErase(0,0x60,0 );
                    __delay_ms(100);
                    VectorRS[0]=0xFE;
                    VectorRS[1]=0x05;// 5 valores
                    VectorRS[2]= ID;
                    VectorRS[3]= orden;
                    VectorRS[4]= Sector;
                    EnviarTX2();
                    break;
               
                    
                case 0xFF:// borrar toda la memoria
                    SectorErase(0,0,0 );
                    __delay_ms(100);
                    VectorRS[0]=0xFE;
                    VectorRS[1]=0x05;// 5 valores
                    VectorRS[2]= ID;
                    VectorRS[3]= orden;
                    VectorRS[4]= Sector;
                    EnviarTX2();
                    break;
               }
                break;
  
        case 0x20:// Falta ID
        U1TXREG = RFBREAD(R_RX_PAYLOAD);
        RFREAD(R_RX_ADDR_P1);
        VectorRS[0] = 0xFE;
        VectorRS[1] = 0x07;
        VectorRS[2]= cachito[0];
        VectorRS[3]= cachito[1];
        VectorRS[4]= cachito[2];
        VectorRS[5]= cachito[3];
        VectorRS[6]= cachito[4];
        EnviarTX2();
        break;
        
        case RECEPTOR:// Falta ID
        U1TXREG = 0xd0;
        aux=RF_RX();               
        VectorRS[0] = 0xFE;
        VectorRS[1] = 0x07;
        VectorRS[2]= cachito[0];
        VectorRS[3]= cachito[1];
        VectorRS[4]= cachito[2];
        VectorRS[5]= cachito[3];
        VectorRS[6]= cachito[4];
//                    VectorRS[2]=aux;
//                    VectorRS[3]=RFBREAD(R_RX_PAYLOAD);
//                    VectorRS[4]=RFBREAD(R_STATUS);
        EnviarTX2();
        break;
                
        case TRANSMISOR:// Falta ID
        U1TXREG = 0xE0;
        RF_TX(0Xdd);
        break;
        
        case LEER_REGISTROS:      //case 0x41 LEER_REGISTROS:
        VectorRS[0]=0xFE;
        VectorRS[1]=0x36;// 54 valores
        VectorRS[2]=ID;// 
        VectorRS[3]= orden;
        for(j=0;j<0x32;j++)
        {
        VectorRS[j+4]=IMUBREAD(j);// Leo todos los registros
         __delay_us(5);
        }
        EnviarTX2();
        break;

        case LEER_ACELERACIONES:        //case 0x42  LEER_REGISTROS:
        VectorRS[0]=0xFE;
        VectorRS[1]=0x0A;// 10 valores
        VectorRS[2]=ID;
        VectorRS[3]= orden;
        for(j=1;j<7;j++)
        {
        VectorRS[j+3]=IMUBREAD(j);//
        __delay_us(5);
        }
        EnviarTX2();
        break;
        
        case ESC_REGISTRO:                //case 0x43 escribir_REGISTROS:
        MMA8471DesActivo();
        __delay_ms(1);
        IMUWRITE(Dire,Valor);
        __delay_ms(1);
        MMA8471Activo();
        VectorRS[0]=0xFE;
        VectorRS[1]=0x04;// 4 valores
        VectorRS[2]= ID;
        VectorRS[3]= orden;
        EnviarTX2();
        break;

        case LECT_FIFO:        //CASE 0x44 Leer aceleraciones de la memoria FIFO
        parar=1;
        MMA8471DesActivo();
        __delay_ms(1);
        IMUWRITE(0x09,0x80);//Fifo circular y 32 muestras
        __delay_ms(1);
        IMUWRITE(0x2D,0x40);//en CTRL_REG4 pongo INT_EN_FIFO=1
        __delay_ms(1);
        IMUWRITE(0x2E,0x40);//en CTRL_REG5 pongo Interrupt is routed to INT1 pin
        __delay_ms(1);
       IMUWRITE(0x2C,0x82); // en CTRL_REG3 FIFO, interrupt active high (rising event)
        __delay_ms(1);
//        IMUWRITE2(cmdW,0x2E,0x00);//  en CTRL_REG5 pongo Interrupt is routed to INT2 pin;
//        IMUWRITE2(cmdW,0x2F,0x00);//  en CTRL_REG3 pongo interrupcion en bajo y push-pull
        MMA8471Activo();
        
        VectorRS[0]=0xFE;
        VectorRS[1]=0xc4;// 196 valores
        VectorRS[3]= ID;
        VectorRS[4]= orden;
        
        while(parar)
        {
        if (PORTAbits.RA8) //Espero que se levante RA8
        {
        IMUBREAD(0x00);// Leo registro F Status
        IMUREAD2(0x01, 192, VectorMMA);// Leo los 192 valores
        EnviarMMA2();
        parar=0; //Fin de rutina FIFO

       MMA8471DesActivo();
        __delay_ms(1);
        IMUWRITE(0x09,0x00);//Sin FIFO
        __delay_ms(1);
        IMUWRITE(0x2D,0x00);//Sin Interrupciones
        __delay_ms(1);
        MMA8471Activo();
        __delay_ms(1);
        }
        }
        break;
        
        case LEER_ACEL_CONT:  //CASE 0x45 Leer aceleraciones en forma continua
        parar=2;
        MMA8471DesActivo();
        __delay_ms(1);
        IMUWRITE(0x09,0x00);//Fifo des habilitada
        __delay_ms(1);
        IMUWRITE(0x2D,0x01);//en CTRL_REG4 pongo INT_EN_DRDY=1
//        IMUWRITE2(cmdW,0x2E,0x00);//  en CTRL_REG5 pongo Interrupt is routed to INT2 pin;
//        IMUWRITE2(cmdW,0x2F,0x00);//  en CTRL_REG3 pongo interrupcion en bajo y push-pull
        __delay_ms(1);
        MMA8471Activo();
      __delay_ms(1);
        VectorRS[0]=0xFE;
        VectorRS[1]=23;// 23 valores
        VectorRS[2]= ID;
        VectorRS[3]= orden;
        Vcuenta=0;
        Trama=0;
        while(parar==2)
        {
        INT_SOURCE=IMUBREAD(0x0C);//leo INT_SOURCE
       
        if ((INT_SOURCE&0x01) == 0x01)// pregunto si hay interrupcion por INT_EN_DRDY
        {
            
   for(j=1;j<7;j++)
        {
        VectorRS[j+3+Vcuenta*6]=IMUBREAD(j);//
        __delay_us(5);
        }
          Vcuenta=Vcuenta+1;
   if (Vcuenta==3)
   {
       VectorRS[22]=Trama;
       EnviarTX2();
        Vcuenta=0;
        Trama=Trama+1;
   } 
   }}
        
        break;
        
        case 0x46:                //parar de leer
        VectorRS[0]=0xFE;
        VectorRS[1]=0x4;// 4 valores
        VectorRS[2]=ID;// 201 valores
        VectorRS[3]= 0x46;
        EnviarTX2();
        MMA8471DesActivo();
        IMUWRITE(0x09,0x00);//des habilito la Fifo
        __delay_ms(1);
        IMUWRITE(0x2D,0x00);//Sin Interrupciones
        __delay_ms(1);
        MMA8471Activo();
        __delay_ms(1);
        
        break;

        case ACEL_LOGGING_ON : //CASE 0x47 Almacenar en memoria la aceleracion
        switch(Bloque)
        {
                case 0x00:// Medicion 1
                    PageL=0;
                    PageH=1;
                    break;
                 
                case 0x01:// Medicion 2
                  PageL=0;
                  PageH=23;
                  break;
                 
                case 0x02:// Medicion 3
                 PageL=0;
                 PageH=45;
                 break;
                 
                case 0x03:// Medicion 4
                PageL=0;
                PageH=67;
                 break;
                 
                case 0x04:// Medicion 5
                 PageL=0;
                 PageH=89;
                 break;
                 
                case 0x05:// Medicion 6
                PageL=0;
                PageH=111;
                 break;
         }
          
            parar=4;
        VectorRS[0]=0xFE;
        VectorRS[1]=4;// 3 valores
        VectorRS[2]= ID;
        VectorRS[3]= orden;
        EnviarTX2();
         MMA8471DesActivo();
        __delay_ms(1);
        IMUWRITE(0x09,0x00);//Fifo des habilitada
        __delay_us(5);
        IMUWRITE(0x2D,0x01);//en CTRL_REG4 pongo INT_EN_DRDY=1
//        IMUWRITE2(cmdW,0x2E,0x00);//  en CTRL_REG5 pongo Interrupt is routed to INT2 pin;
//        IMUWRITE2(cmdW,0x2F,0x00);//  en CTRL_REG3 pongo interrupcion en bajo y push-pull
        __delay_us(5);
        MMA8471Activo();
        __delay_us(5);
        Vcuenta=0;
//        PageH=0; // Por ahora comienzo pagina 0
//        PageL=0;
        while(parar==4)
        {
        INT_SOURCE=IMUBREAD(0x0C);//leo INT_SOURCE
          
        if ((INT_SOURCE&0x01) == 0x01)// pregunto si hay interrupcion por INT_EN_DRDY
        {
           
        for(j=0;j<6;j++)
        {
        VectorMEM[j+Vcuenta*6]=IMUBREAD(j+1);//
         __delay_us(5);
        }
          Vcuenta=Vcuenta+1;
          
   if (Vcuenta==42)//Representa 252 bytes quedan 4 bytes libre
   {
       VectorMEM[252]=PageH;//almaceno el valor alto de la pagina
       VectorMEM[253]=PageL;
       EscribirPagina (PageH, PageL, 0, VectorMEM);// escribo la pagina con datos del acelerometro       
       PageL=PageL+1;
       if (PageL==0){PageH=PageH+1;}
       Vcuenta=0;
   } 
       //   if (PageH==1){parar=0;}//Por ahora termino con 80 segundos
   }}
        break;

        case ACEL_LOGGING_OFF:     //CASE 0x48 Parar de almacenar en memoria la aceleracion
        VectorRS[0]=0xFE;
        VectorRS[1]=0x6;// 6 valores
        VectorRS[2]=ID;// 
        VectorRS[3]= orden;
        VectorRS[4]= PageH;// envio las paginas almacenadas
        VectorRS[5]= PageL;
        EnviarTX2();
        break;
        
        case Leer_ACEL  : //CASE 0x49 Leer datos Almacenados  en memoria 
        
        LeerPagina (DireH, DireL,0,VectorMEM);//Por ahora leo la pagina cero
        VectorRS[0]=0xFE;
        VectorRS[1]=0x04;// 4 valores
        VectorRS[2]=ID;// 
        VectorRS[3]= orden;
        while (U2STAbits.TRMT==0);
        U2TXREG=VectorRS[0];
        while (U2STAbits.TRMT==0);
        U2TXREG= VectorRS[1];
        while (U2STAbits.TRMT==0);
        U2TXREG= VectorRS[2];
        while (U2STAbits.TRMT ==0);
        U2TXREG= VectorRS[3];
        for (j=0;j<256;j++)
        {
        while (U2STAbits.TRMT==0);
        U2TXREG= VectorMEM[j];
        }

        break;
        
        case WhoamI:     //CASE 0x4D Quien soy yo
        VectorRS[0]=0xFE;
        VectorRS[1]=0x5;// 5 valores
        VectorRS[2]=ID;// Quien soy YO
        VectorRS[3]= orden;
        VectorRS[4]=IMUBREAD(0x0D);// Leo todos  el registro 0x0D
        EnviarTX2();
        break;

        case LeerMemD:                //case 0x50 leer un dato memoria serial
//             
        VectorRS[0]=0xFE;
        VectorRS[1]=0x05;// 5 valores
        VectorRS[2]=ID;//envio del ID
        VectorRS[3]= orden;
        VectorRS[4]= LeerDato (DireH,Dire,DireL);//Prueba eb la pagima 0x0100XX
        EnviarTX2();
        break;
        
        case EscMemD:                //case 0x51 escribir un dato memoria serial
        EscribirDato (DireH,Dire,DireL, Valor);//Prueba eb la pagima 0x0100XX
        VectorRS[0]=0xFE;
        VectorRS[1]=0x04;// 4 valores
        VectorRS[2]=ID;//envio del ID
        VectorRS[3]= orden;
        EnviarTX2();
         __delay_ms(25);// espero que se grabe el dato
        break;
        
        case LeerMemID:                //case 0x52 leer ID memoria serial
        //EscribirDato (DireH,Dire,DireL, Valor);//Prueba eb la pagima 0x0100XX
        ReadIdentification (&OUT_X,&OUT_Y,&OUT_Z);
        VectorRS[0]=0xFE;
        VectorRS[1]=0x07;// 3 valores
        VectorRS[2]=ID;//envio del ID
        VectorRS[3]= orden;
        VectorRS[4]= OUT_X;
        VectorRS[5]= OUT_Y;
        VectorRS[6]= OUT_Z;
        EnviarTX2();
        break;
        
         case ResetMem:                //case 0x53 reset memoria serial
        //EscribirDato (DireH,Dire,DireL, Valor);//Prueba eb la pagima 0x0100XX
        ChipErase();
        VectorRS[0]=0xFE;
        VectorRS[1]=0x04;// 3 valores
        VectorRS[2]= ID;//
        VectorRS[3]= orden;
        EnviarTX2();
        break;
        
        case LeerStatusMem:                //case 0x54 Leer Status  memoria serial
        LeerStatus1(&Dato);
        VectorRS[0]=0xFE;
        VectorRS[1]=0x05;// 5 valores
        VectorRS[2]=ID;// 
        VectorRS[3]= orden;
         VectorRS[4]= Dato;
        // VectorRS[3]=LeerStatus();
        EnviarTX2();
        break;
        
        case EscStatusMem:                //case 0x55 Esccribir Status  memoria serial
        EscribirStatus (Valor);
        VectorRS[0]=0xFE;
        VectorRS[1]=0x04;// 4 valores
        VectorRS[2]=ID;// 3 valores
        VectorRS[3]= orden;
        EnviarTX2();
        break;
        
        case UnlockProMem:  //case 0x56 Unlock  memoria serial
        UnlockMem ();  //Global Block Protection Unlock
        VectorRS[0]=0xFE;
        VectorRS[1]=0x04;// 3 valores
        VectorRS[2]=ID;// 
        VectorRS[3]= orden;
        EnviarTX2();
        break;
        
        case LeerConfMem:                //case 0x57 Leer Reg Configuracion memoria serial
        LeerConf1(&Dato);
        VectorRS[0]=0xFE;
        VectorRS[1]=0x05;// 5 valores
        VectorRS[2]=ID;// 
        VectorRS[3]= orden;
         VectorRS[4]= Dato;
        // VectorRS[3]=LeerStatus();
        EnviarTX2();
        break;
        
        case LeerPageMem:                //case 0x58 Leer pagina memoria serial
        LeerPagina (DireH, DireL,0,VectorMEM);//Por ahora leo la pagina cero
        VectorRS[0]=0xFE;
        VectorRS[1]=0x04;// 3 valores
        VectorRS[2]=ID;
        VectorRS[3]= orden;
        while (U2STAbits.TRMT==0);
        U2TXREG=VectorRS[0];
        while (U2STAbits.TRMT==0);
        U2TXREG= VectorRS[1];
        while (U2STAbits.TRMT==0);
        U2TXREG= VectorRS[2];
        while (U2STAbits.TRMT==0);
        U2TXREG= VectorRS[3];
        for (j=0;j<256;j++)
        {
        while (U2STAbits.TRMT==0);
        U2TXREG= VectorMEM[j];
        }
        break;
        
        case EscPageMem: //case 0x59 Leer pagina memoria serial
        for(j=0;j<256;j++)
        {
        VectorMEM[j]=Valor+j;
        }
        EscribirPagina (0, Dire, 0, VectorMEM);//Por ahora escribo la pagina cero
        VectorRS[0]=0xFE;
        VectorRS[1]=0x03;// 4 valores
        VectorRS[2]=ID;//
        VectorRS[3]= orden;
        EnviarTX2();
        break;
        
        case Reset:     //case 0xA5 reset al acelerometro
        MMA8471Reset();
        VectorRS[0]=0xFE;
        VectorRS[1]=0x04;// 3 valores
        VectorRS[2]=ID;// 
        VectorRS[3]= orden;
        EnviarTX2();
        break;
        
/// Falta modificar esta parte de codigo, se debe incorporar el envio del ID        
        
        case MapanRF:     //case 0x83 leo mapa de registro del nRF
        VectorRS[0]=0xFE;
        VectorRS[1]=0x28;// 40 valores
        VectorRS[2]= orden;
        //Agregar lectura SPI3
        
        CS_RF=0; // Bajo el CS del nRF
        //Para leer un registro R_REGISTER=000A AAAA
                    
        for(i=0;i<10;i++){
        CS_RF=0;
        WByteSPI3(i);//Leo distintos registros direccion 0 a 9   
        VectorRS[i+3] = RByteSPI3();	
        CS_RF=1; // Levanto el CS del nRF
        }
        CS_RF=0;
        WByteSPI3(0x0A);//Leo  registro RX_ADDR_P0    
        VectorRS[13] = RByteSPI3();	
        VectorRS[14] = RByteSPI3();//
        VectorRS[15] = RByteSPI3();	
        VectorRS[16] = RByteSPI3();//	
        VectorRS[17] = RByteSPI3();
        CS_RF=1; // Levanto el CS del nRF 
        __delay_us(10);
        CS_RF=0;
        WByteSPI3(0x0B);// Leo  registro RX_ADDR_P1  
        VectorRS[18] = RByteSPI3();	
        VectorRS[19] = RByteSPI3();//
        VectorRS[20] = RByteSPI3();	
        VectorRS[21] = RByteSPI3();//	
        VectorRS[22] = RByteSPI3();
        CS_RF=1; // Levanto el CS del nRF 
        __delay_us(10);
        CS_RF=0;
        WByteSPI3(0x0C);//Leo  registro RX_ADDR_P2    
        VectorRS[23] = RByteSPI3();	
        CS_RF=1; // Levanto el CS del nRF
        __delay_us(10);
        CS_RF=0;
        WByteSPI3(0x0D);//Leo  registro RX_ADDR_P3 
        VectorRS[24] = RByteSPI3();	
        CS_RF=1; // Levanto el CS del nRF
        __delay_us(10);
        CS_RF=0;
        WByteSPI3(0x0E);//Leo  registro RX_ADDR_P4   
        VectorRS[25] = RByteSPI3();	
        CS_RF=1; // Levanto el CS del nRF
        __delay_us(10);
        CS_RF=0;
        WByteSPI3(0x0F);//Leo  registro RX_ADDR_P5   
        VectorRS[26] = RByteSPI3();	
        CS_RF=1; // Levanto el CS del nRF
        __delay_us(10);
        CS_RF=0;
        WByteSPI3(0x10);//Leo  registro TX_ADDR    
        VectorRS[27] = RByteSPI3();	
        VectorRS[28] = RByteSPI3();//
        VectorRS[29] = RByteSPI3();	
        VectorRS[30] = RByteSPI3();//	
        VectorRS[31] = RByteSPI3();
        CS_RF=1; // Levanto el CS del nRF 
                    
        for(i=0x11;i<0x18;i++){
        CS_RF=0;
        WByteSPI3(i);//Leo distintos registros direccion 17 a 23   
        VectorRS[i+15] = RByteSPI3();	
        CS_RF=1; // Levanto el CS del nRF
        }
        __delay_us(10); 
        CS_RF=0;
        WByteSPI3(0x1c);//Leo registro DYNPD 
        VectorRS[39] = RByteSPI3();	
        CS_RF=1; // Levanto el CS del nRF
        __delay_us(10); 
        CS_RF=0;
        WByteSPI3(0x1d);//Leo registro Feature 
        VectorRS[40] = RByteSPI3();	
        CS_RF=1; // Levanto el CS del nRF
        EnviarTX2();
        break;
        
        
        case WReg1nRF:
        VectorRS[0]=0xFE;
        VectorRS[1]=0x3;// 3 valores
        VectorRS[2]= orden;
        CS_RF=0; // Bajo el CS del nRF
        Dire=Dire|0x20;
        WByteSPI3(Dire);//Direccion
        WByteSPI3(Valor);//Dato
        __delay_us(1);	
        CS_RF=1; // Levanto el CS del nRF
        EnviarTX2();
        break;
        
        case wReg5nRF:   //escribir cinco registro nRF. 0x86
        VectorRS[0]=0xFE;
        VectorRS[1]=0x3;// 35 valores
        VectorRS[2]= orden;            
                    CS_RF=0; // Bajo el CS del nRF
                    Vector[0]=Vector[0]|0x20;
                    WByteSPI3(Vector[0]);//Direccion
                    WByteSPI3(Vector[1]);//Dato
                    WByteSPI3(Vector[2]);//Dato
                    WByteSPI3(Vector[3]);//Dato
                    WByteSPI3(Vector[4]);//Dato
                    WByteSPI3(Vector[5]);//Dato
                    __delay_us(1);	
                    CS_RF=1; // Levanto el CS del nRFLeerFIFOnRF
                    EnviarTX2();           
        break;
        
        case LeerFIFOnRF:     //case 0x87 leo FIFO de registro del nRF
        VectorRS[0]=0xFE;
        VectorRS[1]=0x23;// 3 valores
        VectorRS[2]= orden;
              
        CS_RF=0; // Bajo el CS del nRF
        //Para leer un registro R_REGISTER=000A AAAA
        WByteSPI3(0x61);//
        for(i=3;i<35;i++){
        VectorRS[i] = RByteSPI3();	
        }
        CS_RF=1; // Levanto el CS del nRF
        EnviarTX2();
        break;
        
        case EscFIFOnRFACK:   //escribir FIFO con ACK nRF. 0x88
        CS_RF=0; // Bajo el CS del nRF
        WByteSPI3(0xA0);//
        for(i=0;i<Nbytes-3;i++){
        WByteSPI3(VectorRS[i]);	
        }
        __delay_us(1);
        CS_RF=1; // Levanto el CS del nRF
        __delay_us(1);
        CE_RF=1;
        __delay_us(20);
        CE_RF=0;
        VectorRS[0]=0xFE;
        VectorRS[1]=0x3;// 3 valores
        VectorRS[2]= orden; 
        EnviarTX2();           
        break;
        
        case RXnRF:   //recepcion al RF. 0x89
        VectorRS[0]=0xFE;
        VectorRS[1]=0x3;// 3 valores
        VectorRS[2]= orden; 
        CS_RF=0; // Bajo el CS del nRF 
        WByteSPI3(0x00);//Leo  registro CONFIG    
        datt = RByteSPI3();	
        datt=datt|0x3;//POWER UP y RX/TX en RX en uno
        CS_RF=1; // Levanto el CS del nRF
        __delay_us(10);
        CS_RF=0; // Bajo el CS del nRF 
        WByteSPI3(0x20);//Direcciono registro CONFIG
        WByteSPI3(datt);
        __delay_us(10);	
        CS_RF=1; // Levanto el CS del nRF
        CE_RF=1;//Enciendo el equipo de RADIO
        EnviarTX2();           
        break;
        
        case TXnRF:   //transmicion al nRF. 0x8A
        VectorRS[0]=0xFE;
        VectorRS[1]=0x3;// 3 valores
        VectorRS[2]= orden; 
        CS_RF=0; // Bajo el CS del nRF 
        WByteSPI3(0x00);//Leo  registro CONFIG    
        datt = RByteSPI3();	
        datt=datt|0x02;//POWER UP en 1 
        datt=datt&0xFE;// RX/TX en TX en cero
        CS_RF=1; // Levanto el CS del nRF
        __delay_us(10);
        CS_RF=0; // Bajo el CS del nRF 
        WByteSPI3(0x20);//Direcciono registro CONFIG
        WByteSPI3(datt);
        __delay_us(1);	
        CS_RF=1; // Levanto el CS del nRF
        CE_RF=0;//Enciendo el equipo de RADIO
        EnviarTX2();           
        break;
        
        case SBynRF:   //StandBy al RF. 0x8B
        VectorRS[0]=0xFE;
        VectorRS[1]=0x3;// 3 valores
        VectorRS[2]= orden; 
        CS_RF=0; // Bajo el CS del nRF 
        WByteSPI3(0x00);//Leo  registro CONFIG    
        datt=datt&0xFC;//POWER UP y RX/TX en RX en cero
        CS_RF=1; // Levanto el CS del nRF
        __delay_us(10);
        CS_RF=0; // Bajo el CS del nRF 
        WByteSPI3(0x20);//Direcciono registro CONFIG
        WByteSPI3(datt);
        __delay_us(10);	
        CS_RF=1; // Levanto el CS del nRF
        __delay_us(1);
        CE_RF=0;//Enciendo el equipo de RADIO
        EnviarTX2();           
        break;
        
        case EscFIFOnRFNACK:   //escribir FIFO sin ACK nRF. 0x8D
                    
        CS_RF=0; // Bajo el CS del nRF
        WByteSPI3(0xB0);//
        for(i=0;i<Nbytes-3;i++){
        WByteSPI3(VectorRS[i]);	
        }
        __delay_us(1);	
        CS_RF=1; // Levanto el CS del nRF
        __delay_us(1);	
        CE_RF=1;
        __delay_us(20);
        CE_RF=0;
        VectorRS[0]=0xFE;
        VectorRS[1]=0x3;// 3 valores
        VectorRS[2]= orden;
        EnviarTX2();           
        break;
        
        case CFIFORXnRF:   //Limpiar FIFO RX nRF. 0x8E
        VectorRS[0]=0xFE;
        VectorRS[1]=0x3;// 3 valores
        VectorRS[2]= orden;            
        CS_RF=0; // Bajo el CS del nRF
        WByteSPI3(0xE2);//
        __delay_us(1);	
        CS_RF=1; // Levanto el CS del nRF
        EnviarTX2();           
        break;
        
        case CFIFOTXnRF:   //Limpiar FIFO RX nRF. 0x8F
        VectorRS[0]=0xFE;
        VectorRS[1]=0x3;// 3 valores
        VectorRS[2]= orden;            
        CS_RF=0; // Bajo el CS del nRF
        WByteSPI3(0xE1);//
        __delay_us(1);	
        CS_RF=1; // Levanto el CS del nRF
        EnviarTX2();           
        break;
        
        case PruINT:        //case 0xAB Prueba de lectura de las aceleraciones con Interrupciones:
        MMA8471DesActivo();
        __delay_ms(10);
        IMUWRITE(0x2D,0x01);//en CTRL_REG4 pongo INT_EN_DRDY=1
        IMUWRITE(0x2E,0x01);//en CTRL_REG5 pongo INT_CFG_DRDY=1
        MMA8471Activo();//Ponemos al MMA en modo activo
        IntInit(); //Habilito las interrupciones por INT1
        break;
        
        default: 
        Nop();
        break;
        }  // Switch USAER2           
      
}         // if Kbit de USART2

      }// While(1))
}//main

/********************************************************************************/
void Config1(void)// Configuraciób de puertos y otros
{
    #define FCY 60000000UL//
    #define BAUDRATE 19200
    #define BRGVAL ((FCY/BAUDRATE)/4) - 1//con
    // Configure Oscillator to operate the device at 60 MHz
    // Fosc = Fin * M/(N1 * N2), Fcy = Fosc/2
    // Fosc = 4M * 120/(2 * 2) = 60 MHz for 4M input clock
    // Fosc = 8M * 60/(2 * 2) = 60 MHz for 8M input clock
    //valores frec para XTPLL hasta 8MHz , para valores mayores HS 
    PLLFBD = 58;//58;//118;			// M = 60
	CLKDIVbits.PLLPOST =0;		// N2 = 4
	CLKDIVbits.PLLPRE = 0; 		// N1 = 2
    /* Para tener Fcy=60Mhz
     Para cristal de 4Mhz hay que poner PLLPOST=0 , PLLPOST=0 y PLLFBD=118
     Para cristal de 8Mhz hay que poner PLLPOST=0 , PLLPOST=0 y PLLFBD=58
     */
    OSCTUN = 0;                                 // Tune FRC oscillator, if FRC is used
    RCONbits.SWDTEN = 0;                        /* Disable Watch Dog Timer*/

	__builtin_write_OSCCONH(0x03);	// Initiate Clock Switch to Primary Oscillator with PLL (NOSC = 0b011)
	__builtin_write_OSCCONL(OSCCON | 0x01);//__builtin_write_OSCCONL(0x01);
	Nop();
    while(OSCCONbits.COSC != 0b011);// Wait for Clock switch to occur
	while(OSCCONbits.LOCK != 1){};	// Wait for PLL to lock
    
//	PORTB = 0;
    ANSELA=0x0000;// Sin entradas analógicas
    ANSELB=0x0000;
    ANSELC=0x0000;
    TRISAbits.TRISA9=1; //MISO SPI1     
    TRISCbits.TRISC3=0; // Clock SPI1
    TRISAbits.TRISA1=0; //MOSI SPI1    
    TRISAbits.TRISA8=1 ; //Entrada INT1 del acelerómetro
    TRISCbits.TRISC4=1 ; //Entrada INT2 del acelerómetro      
    TRISBbits.TRISB2=1; //MISO SPI2     
    TRISBbits.TRISB3=0; // Clock SPI2
    TRISCbits.TRISC1=0; //MOSI SPI2
    TRISBbits.TRISB4=0;	// CS del FX
    CS_FX=1;             // Levanto el CS del FX  
	TRISAbits.TRISA1=0;	// CS de la memoria serial 
    TRISCbits.TRISC0=0; //CS SD SPI2
    CS_M=1;      // Levanto el CS de la memoria  
    CS_SD=1;      // Levanto el CS de la memoria  SD
    TRISBbits.TRISB8 = 1;	// RX USART1
	TRISBbits.TRISB7 = 0;	//TX  USART1
    
    TRISCbits.TRISC5 = 1;	//RX_MISO APC, TX para Micro
	TRISBbits.TRISB5 = 0;	//TX_MOSI  APC, RX para micro
    
    TRISAbits.TRISA8=1; //Es para FX_INT1
    TRISCbits.TRISC4=1; //Es para FX_INT2
//    //*******Configuración pines del nRF
//    TRISCbits.TRISC9=1; //MISO SPI3     
//    TRISCbits.TRISC8=0;// Clock SPI3
//    TRISBbits.TRISB5=0; //MOSI SPI3 
//    TRISBbits.TRISB6=0;	// CSN del nRF24l01
//    TRISCbits.TRISC7=0;	// CE del nRF24l01
//    CE_RF=0;
//    __delay_ms(1);
//    CS_RF=1;
    
//	
        
    OSCCON = 0x46;			// Command Sequence
	OSCCON = 0x57;
    OSCCONbits.IOLOCK = 0;		// Peripherial pin select is not locked

    RPOR4bits.RP43R=0b00001;        // UART1_TX -> RP43 
    RPINR18bits.U1RXR = 0b101100;	// UART1_RX -> RP44
    // Configuración de Pines para USART2 , es para APC
    //RPOR1bits.RP37R=0b00011;        // UART2_TX -> RP37 
    //RPINR19bits.U2RXR = 0b0110100;	// UART2_RX -> RPI52
    RPOR1bits.RP37R=0b00011;        // UART2_TX -> RP37 
    RPINR19bits.U2RXR = 0b0110101;	// UART2_RX -> RPI53
    // Configuración de Pines para SPI2 , es para memoria serial
    RPOR5bits.RP49R = 0b00001000;	// MOSI  SDO2 -> RP49
    RPOR0bits.RP35R = 0b00001001;	// clock Out CLK2 -> RP35
    RPINR22bits.SDI2R = 0b00100010;	// MISO  SDI2 -> RPI34 010 0011
    RPINR22bits.SCK2R=  0b00100011; // clock In CLK2 -> RP35 Por las dudas
	
//    //Configuración de SPI3 (Idem PanchoMaqui))
//    RPOR7bits.RP56R =   0b00100000; //clock del spi3 -> RP56
//    RPOR1bits.RP37R =   0b00011111; //MOSI --> RP37
//    RPINR0bits.INT1R =  0b00101010; //IRQ -> INT0 -> RP42
//    RPINR29bits.SDI3R = 0b00111001; //MISO -> RP57
//    RPINR29bits.SCK3R=  0b0111000;//clock del spi como entrada tambien
//    RPINR0bits.INT1R=   0b00011000;// Asignar a pin RA8 (RPI24) como INT1
     // Transceiver Configuration
		
        #define PHY_CS              LATBbits.LATB2
        #define PHY_CS_TRIS         TRISBbits.TRISB2
        #define PHY_RESETn          LATGbits.LATG2
        #define PHY_RESETn_TRIS     TRISGbits.TRISG2
        #define PHY_WAKE            LATGbits.LATG3
        #define PHY_WAKE_TRIS       TRISGbits.TRISG3
        // SPI Configuration
        #define SPI_SDI             PORTFbits.RF7
        #define SDI_TRIS            TRISFbits.TRISF7
        #define SPI_SDO             LATFbits.LATF8 
        #define SDO_TRIS            TRISFbits.TRISF8
        #define SPI_SCK             LATFbits.LATF6 
        #define SCK_TRIS            TRISFbits.TRISF6

        #define GetInstructionClock()	(CLOCK_FREQ/2)
       
	OSCCON = 0x46;			// Command Sequence
    OSCCON = 0x57;
	OSCCONbits.IOLOCK = 1;		// Peripherial pin select is locked

	// UART1 CONFIGURATION
	U1MODEbits.PDSEL = 0b00;	// 8-bit data, no parity
	U1MODEbits.STSEL = 0;		// One Stop bit
	U1MODEbits.ABAUD = 0;		// Auto-Baud DISABLED
	U1MODEbits.BRGH = 1;		// Low-speed mode
	U1BRG = BRGVAL;//   Valor inicial para XTPLL=86; Baud Rate - 19200 bps
	U1MODEbits.USIDL = 0;		// Continue module operation in Idle mode
	U1MODEbits.IREN = 0;		// IrDA® encoder and decoder disabled
	U1MODEbits.WAKE = 0;		// No wake-up enabled
	U1MODEbits.LPBACK = 0;		// Loopback mode is disabled
	U1MODEbits.URXINV = 0;		// U1RX Idle state is '1'
	U1STAbits.UTXINV = 0;		// UxTX Idle state is ‘1’
	U1STAbits.OERR = 0;		    // Clear Overrun Error
	U1STAbits.ADDEN = 0;		// Address Detect mode disabled
	U1STAbits.UTXBRK = 0;		// Sync Break transmission disabled or completed
	U1STAbits.UTXISEL0 = 0;
	U1STAbits.UTXISEL1 = 0;
	U1STAbits.URXISEL = 0;
	IFS0bits.U1RXIF = 0;// Limpio la bandera de la interrupción
	IEC0bits.U1RXIE = 1;//Habilito la interrupción por recepción RX
	U1MODEbits.UEN = 0;		// UxTX and UxRX pins are enabled and used;
    // UxCTS and UxRTS/BCLK pins controlled by port latches
	U1MODEbits.UARTEN = 1;		// UART1 is enabled
	U1STAbits.UTXEN = 1;		// Transmit Enabled
    
    // UART2 CONFIGURATION
	U2MODEbits.PDSEL = 0b00;	// 8-bit data, no parity
	U2MODEbits.STSEL = 0;		// One Stop bit
	U2MODEbits.ABAUD = 0;		// Auto-Baud DISABLED
	U2MODEbits.BRGH = 1;		// Low-speed mode
	U2BRG = BRGVAL;//   Valor inicial para XTPLL=86; Baud Rate - 19200 bps
	U2MODEbits.USIDL = 0;		// Continue module operation in Idle mode
	U2MODEbits.IREN = 0;		// IrDA® encoder and decoder disabled
	U2MODEbits.WAKE = 0;		// No wake-up enabled
	U2MODEbits.LPBACK = 0;		// Loopback mode is disabled
	U2MODEbits.URXINV = 0;		// U1RX Idle state is '1'
	U2STAbits.UTXINV = 0;		// UxTX Idle state is ‘1’
	U2STAbits.OERR = 0;		    // Clear Overrun Error
	U2STAbits.ADDEN = 0;		// Address Detect mode disabled
	U2STAbits.UTXBRK = 0;		// Sync Break transmission disabled or completed
	U2STAbits.UTXISEL0 = 0;
	U2STAbits.UTXISEL1 = 0;
	U2STAbits.URXISEL = 0;
	IFS1bits.U2RXIF = 0;// Limpio la bandera de la interrupción
	IEC1bits.U2RXIE = 1;//Habilito la interrupción por recepción RX
	U2MODEbits.UEN = 0;		// UxTX and UxRX pins are enabled and used;
    // UxCTS and UxRTS/BCLK pins controlled by port latches
	U2MODEbits.UARTEN = 1;		// UART1 is enabled
	U2STAbits.UTXEN = 1;		// Transmit Enabled

}

/* ****************Interrupciones*********************************************/
void IntInit(void)
{
   INTCON2bits.INT1EP = 1;   //Flanco decreciente para INT1
//   IFS0bits.INT0IF = 0;    /*Reset INT0 interrupt flag */
//   IPC0=0x07;// Alta prioridad para INT0
//   IEC0bits.INT0IE = 1;    /*Enable INT0 Interrupt Service Routine */
//
//   TRISBbits.TRISB7=1;// INT0 entrada
   IPC5bits.INT1IP=3;// Nivel de prioridad de INT1
   IFS1bits.INT1IF=0;//Limpio bandera
   IEC1bits.INT1IE=1;  //Habilito las interrupcion de INT1       
}

void __attribute__((interrupt,no_auto_psv)) _INT1Interrupt(void)
{
   //printf("s");
    VectorRS[0]=0xFE;
    VectorRS[1]=0x09;// 9 valores
    VectorRS[2]= orden;
    for(j=1;j<7;j++)
    {
    VectorRS[j+2]=IMUBREAD(j);//
    __delay_us(5);
    }
    EnviarTX();
   IFS1bits.INT1IF = 0;    //Limpio bandera de INT1 

   
        }


   void __attribute__((interrupt, no_auto_psv)) _U1RXInterrupt( void )
 {
       int dato,j;
       
 dato=U1RXREG;
  if (dato==0xFD)
 {
 while (!DataRdyUART1());
 Nbytes=U1RXREG;//leo la cantidad de bytes que llegan por el puerto serie
 while (!DataRdyUART1());
 ID=U1RXREG;//leo identificacion de dispositivo
 while (!DataRdyUART1());
 orden=U1RXREG;
 switch (orden)
 {
     case 0x03: //broadcast comando lectura por tiempo llegan 8 bytes
     while (!DataRdyUART1());
    char2long.aa[2] =U1RXREG;
     while (!DataRdyUART1());
    char2long.aa[1] =U1RXREG;
     while (!DataRdyUART1());
    char2long.aa[0] =U1RXREG;
    while (!DataRdyUART1());
    ID1=U1RXREG;
//    while (!DataRdyUART1());
//    Bloque =U1RXREG;
    if (ID==0 && (ID1&1<<sensor)!=0x00)Kbhit=1;
    parar=1;
    break;
     
    case 0x04: //broadcast trigger llegan 11 bytes
    while (!DataRdyUART1());
    Master=U1RXREG; //Define quien es el master
    while (!DataRdyUART1());
    Trigger=U1RXREG;  //Valor del trigger
    while (!DataRdyUART1());
    char2long.aa[2] =U1RXREG;
     while (!DataRdyUART1());
    char2long.aa[1] =U1RXREG;
     while (!DataRdyUART1());
    char2long.aa[0] =U1RXREG;
    while (!DataRdyUART1());
    muestras_C=U1RXREG;  //cantidad de páginas
    while (!DataRdyUART1());
    ID1=U1RXREG;
    if (ID==0 && (ID1&1<<sensor)!=0x00)Kbhit=1;
    if (Master==sensor)
        {Master=1;}
    else
        {Master=0;}
    break;
    
    case 0x05: //Borrado memoria
         while (!DataRdyUART1());
        Bloque=U1RXREG;
         break;
         
     case 0x06:
         while (!DataRdyUART1());
        sensor=U1RXREG;
         EscribirDato (0,0,0, sensor);//Prueba eb la pagima 0x0100XX
         break;
    
    case 0x07:
         while (!DataRdyUART1());
        Bloque=U1RXREG;
        break;
        
    case 0x08: //Borrado sector memoria
    while (!DataRdyUART1());
    Sector=U1RXREG;
    break;
    
     
     case 0x10://Si llega la orden 0x10 inicio la conversion de la memoria seria ala SD e indica cual de las 8 lectura es
                {
                    while (!DataRdyUART1());
                    Valor = U1RXREG; //Indica que lectura es

                    while (!DataRdyUART1());
                    tiempo[0] = U1RXREG; //Indica el mes

                    while (!DataRdyUART1());
                    tiempo[1] = U1RXREG; //Indica el dia

                    while (!DataRdyUART1());
                    tiempo[2] = U1RXREG; //Indica el ano

                    while (!DataRdyUART1());
                    tiempo[3] = U1RXREG; //Indica la hora

                    while (!DataRdyUART1());
                    tiempo[4] = U1RXREG; //Indica los minutos

                    while (!DataRdyUART1());
                    tiempo[5] = U1RXREG; //Indica los segundos
                    break;
                }

     case 0x43:// llegan 5 bytes
 {
 while (!DataRdyUART1());
 Dire=U1RXREG;
 while (!DataRdyUART1());
 Valor=U1RXREG;
 break;
 }
 
  case 0x46://Si llega la orden 0x46 o 0x48 deja de enviar o almacenar datos
 {
     parar=0;
     break;
 }
 
  case 0x47: //  llegan 6 bytes inicio delectura y almacenamiento
 {
 while (!DataRdyUART1());
 Bloque=U1RXREG;
// while (!DataRdyUART1());
// PageLi=U1RXREG;
 break;
 }
  
 case 0x48://Si llega la orden 0x46 o 0x48 deja de enviar o almacenar datos
 {
     parar=0;
     break;
 }
 case 0x49://Si llega la orden 0x49 indico que pagina leo
 {
     while (!DataRdyUART1());
    DireH=U1RXREG;
    while (!DataRdyUART1());
    DireL=U1RXREG;
   
     break;
 }
  case 0x50:// llegan 6 bytes por ahora
 {
 while (!DataRdyUART1());
 DireH=U1RXREG;
 while (!DataRdyUART1());
 Dire=U1RXREG;
 while (!DataRdyUART1());
 DireL=U1RXREG;
 break; 
 }
     
  case 0x51:// llegan 7 bytes
 {
 while (!DataRdyUART1());
 DireH=U1RXREG;
 while (!DataRdyUART1());
 Dire=U1RXREG;
 while (!DataRdyUART1());
 DireL=U1RXREG;
 while (!DataRdyUART1());
 Valor=U1RXREG;
 break;
 }
  
  case 0x55: //  llegan 4 bytes
 {
 while (!DataRdyUART1());
 Valor=U1RXREG;
 break;
 }
 
   case 0x58: //  llegan 4 bytes
 {
 while (!DataRdyUART1());
 DireH=U1RXREG;
 while (!DataRdyUART1());
 DireL =U1RXREG;
 break;
 }
  
  case 0x59: //  llegan 5 bytes
 {
 while (!DataRdyUART1());
 Dire=U1RXREG;
 while (!DataRdyUART1());
 Valor=U1RXREG;
 break;
 }
 
  case 0x85: //  llegan 5 bytes pata nRF
 {
 while (!DataRdyUART1());
 Dire=U1RXREG;
 while (!DataRdyUART1());
 Valor=U1RXREG;
 break;
 }
  case 0x86: //  llegan 9 bytes pata nRF
 {
 while (!DataRdyUART1());
 Vector[0]=U1RXREG;
 while (!DataRdyUART1());
 Vector[1]=U1RXREG;
 while (!DataRdyUART1());
 Vector[2]=U1RXREG;
 while (!DataRdyUART1());
 Vector[3]=U1RXREG;
 while (!DataRdyUART1());
 Vector[4]=U1RXREG;
 while (!DataRdyUART1());
 Vector[5]=U1RXREG;
 break;
 }
case 0x88: //  llegan Nbytes bytes pata nRF
 {
  for(j=0;j<Nbytes-3;j++){
  while (!DataRdyUART1());
 VectorRS[j]=U1RXREG; 
  }
 break;
  }
 case 0x8D: //  llegan 35 bytes pata nRF
 {
  for(j=0;j<Nbytes-3;j++){
  while (!DataRdyUART1());
 VectorRS[j]=U1RXREG; 
  }
 break;
  }
 default:
 Nop();
 break; 
 }
 if (ID==sensor)// Veo si coincide con mi ID 
 {
 Kbhit=1;
 }
 }
 IFS0bits.U1RXIF = 0; // Clear RX Interrupt flag
 }

 
   void __attribute__((interrupt, no_auto_psv)) _U2RXInterrupt( void )
 {
       int dato,j;
       
 dato=U2RXREG;
  if (dato==0xFD)
 {
 while (!DataRdyUART2());
 Nbytes=U2RXREG;//leo la cantidad de bytes que llegan por el puerto serie
 while (!DataRdyUART2());
 ID=U2RXREG;//leo identificacion de dispositivo
 while (!DataRdyUART2());
 orden=U2RXREG;
 switch (orden)
 {
     case 0x01: // Trigger empezar a almacenar
         if (ID==0)cancelar=0;
         break;
         
     case 0x03: //broadcast comando lectura por tiempo llegan 8 bytes
     while (!DataRdyUART2());
    char2long.aa[2] =U2RXREG;
     while (!DataRdyUART2());
    char2long.aa[1] =U2RXREG;
     while (!DataRdyUART2());
    char2long.aa[0] =U2RXREG;
    while (!DataRdyUART2());
    ID1=U2RXREG;
    if (ID==0 && (ID1&1<<sensor)!=0x00)Kbhit2=1;
    parar=1; 
    
    break;
     
    case 0x04: //broadcast trigger llegan 11 bytes
    while (!DataRdyUART2());
    Master=U2RXREG; //Define quien es el master
    while (!DataRdyUART2());
    Trigger=U2RXREG;  //Valor del trigger
    while (!DataRdyUART2());
    char2long.aa[2] =U2RXREG;
     while (!DataRdyUART2());
    char2long.aa[1] =U2RXREG;
     while (!DataRdyUART2());
    char2long.aa[0] =U2RXREG;
    while (!DataRdyUART2());
    muestras_C=U2RXREG;  //cantidad de páginas
    while (!DataRdyUART2());
    ID1=U2RXREG;
    if (ID==0 && (ID1&1<<sensor)!=0x00)Kbhit2=1;//if (ID==0 && ID1==0x02)Kbhit2=1;
    if (Master==sensor)
        {Master=1;}
    else
        {Master=0;}
    break;
    
     case 0x05: //Borrado memoria
         while (!DataRdyUART2());
        Bloque=U2RXREG;
         break;
         
    case 0x06:
         while (!DataRdyUART2());
        sensor=U2RXREG;
         EscribirDato (0,0,0, sensor);//Prueba eb la pagima 0x0100XX
         break;
    
    case 0x07:
         while (!DataRdyUART2());
        Bloque=U2RXREG;
        break;
        
    case 0x08: //Borrado sector memoria
    while (!DataRdyUART2());
    Sector=U2RXREG;
    break;
    
    case 0x10://Si llega la orden 0x10 inicio la conversion de la memoria seria ala SD e indica cual de las 8 lectura es
                {
                    while (!DataRdyUART2());
                    Valor = U2RXREG; //Indica que lectura es

                    while (!DataRdyUART2());
                    tiempo[0] = U2RXREG; //Indica el mes

                    while (!DataRdyUART2());
                    tiempo[1] = U2RXREG; //Indica el dia

                    while (!DataRdyUART2());
                    tiempo[2] = U2RXREG; //Indica el ano

                    while (!DataRdyUART2());
                    tiempo[3] = U2RXREG; //Indica la hora

                    while (!DataRdyUART2());
                    tiempo[4] = U2RXREG; //Indica los minutos

                    while (!DataRdyUART2());
                    tiempo[5] = U2RXREG; //Indica los segundos
                    break;
                }
         
     case 0x43:// llegan 5 bytes
 {
 while (!DataRdyUART2());
 Dire=U2RXREG;
 while (!DataRdyUART2());
 Valor=U2RXREG;
 break;
 }
 
  case 0x46://Si llega la orden 0x46 o 0x48 deja de enviar o almacenar datos
 {
     parar=0;
     break;
 }
 
   case 0x47: //  llegan 6 bytes inicio delectura y almacenamiento
 {
 while (!DataRdyUART2());
 Bloque=U2RXREG;
// while (!DataRdyUART1());
// PageLi=U1RXREG;
 break;
 }
 case 0x48://Si llega la orden 0x46 o 0x48 deja de enviar o almacenar datos
 {
     parar=0;
     break;
 }
 case 0x49://Si llega la orden 0x49 indico que pagina leo
 {
     while (!DataRdyUART2());
    DireH=U2RXREG;
    while (!DataRdyUART2());
    DireL=U2RXREG;
   
     break;
 }
  case 0x50:// llegan 6 bytes por ahora
 {
 while (!DataRdyUART2());
 DireH=U2RXREG;
 while (!DataRdyUART2());
 Dire=U2RXREG;
 while (!DataRdyUART2());
 DireL=U2RXREG;
 break; 
 }
     
  case 0x51:// llegan 7 bytes
 {
 while (!DataRdyUART2());
 DireH=U2RXREG;
 while (!DataRdyUART2());
 Dire=U2RXREG;
 while (!DataRdyUART2());
 DireL=U2RXREG;
 while (!DataRdyUART2());
 Valor=U2RXREG;
 break;
 }
  
  case 0x55: //  llegan 4 bytes
 {
 while (!DataRdyUART2());
 Valor=U2RXREG;
 break;
 }
 
   case 0x58: //  llegan 4 bytes
 {
 while (!DataRdyUART2());
 DireH=U2RXREG;
 while (!DataRdyUART2());
 DireL =U2RXREG;
 break;
 }
  
  case 0x59: //  llegan 5 bytes
 {
 while (!DataRdyUART2());
 Dire=U2RXREG;
 while (!DataRdyUART2());
 Valor=U2RXREG;
 break;
 }
 
  case 0x85: //  llegan 5 bytes pata nRF
 {
 while (!DataRdyUART2());
 Dire=U2RXREG;
 while (!DataRdyUART2());
 Valor=U2RXREG;
 break;
 }
  case 0x86: //  llegan 9 bytes pata nRF
 {
 while (!DataRdyUART2());
 Vector[0]=U2RXREG;
 while (!DataRdyUART2());
 Vector[1]=U2RXREG;
 while (!DataRdyUART2());
 Vector[2]=U2RXREG;
 while (!DataRdyUART2());
 Vector[3]=U2RXREG;
 while (!DataRdyUART2());
 Vector[4]=U2RXREG;
 while (!DataRdyUART2());
 Vector[5]=U2RXREG;
 break;
 }
case 0x88: //  llegan Nbytes bytes pata nRF
 {
  for(j=0;j<Nbytes-3;j++){
  while (!DataRdyUART2());
 VectorRS[j]=U2RXREG; 
  }
 break;
  }
 case 0x8D: //  llegan 35 bytes pata nRF
 {
  for(j=0;j<Nbytes-3;j++){
  while (!DataRdyUART2());
 VectorRS[j]=U2RXREG; 
  }
 break;
  }
 default:
 Nop();
 break; 
 }
 if (ID==sensor)// Veo si coincide con mi ID 
 {
 Kbhit2=1;
 }
 }
 IFS1bits.U2RXIF = 0; // Clear RX Interrupt flag
 }

   void EnviarTX(void)
   {
       unsigned int i, cant;
       cant=VectorRS[1];
       for (i=0;i<cant;i++)
       {
       while (U1STAbits.TRMT==0);
       //while(IFS0bits.U1TXIF);
       U1TXREG= VectorRS[i];
       Nop();
       IFS0bits.U1TXIF=0;
       }

   }
   
   void EnviarTX2(void)
   {
       unsigned int i, cant;
       cant=VectorRS[1];
       for (i=0;i<cant;i++)
       {
       while (U2STAbits.TRMT==0);
       //while(IFS0bits.U1TXIF);
       U2TXREG= VectorRS[i];
       Nop();
       IFS1bits.U2TXIF=0;
       }

   }
   

 void MMA8471Activo(void)
 {
     CTRL_REG1=IMUBREAD(0x2A);//Leo el registro 0x2A
     CTRL_REG1=CTRL_REG1 | 0x01;//seteo el bit ACTIVE
      __delay_ms(1);
     IMUWRITE(0x2A,CTRL_REG1);// MMA se pone en modo activo
//     IMUWRITE(0x2A,0x01);// MMA se pone en modo desactivo
 }
 
 void MMA8471DesActivo(void)
 {
     CTRL_REG1=IMUBREAD(0x2A);//Leo el registro 0x2A
     CTRL_REG1=CTRL_REG1 & 0xFE;//Limpio el bit ACTIVE
      __delay_ms(1);
     IMUWRITE(0x2A,CTRL_REG1);// MMA se pone en modo desactivo
//     IMUWRITE(0x2A,0x00);// MMA se pone en modo desactivo
 }

 void MMA8471Reset(void)//Genero un Reset en el acelerometro
 {
    CTRL_REG1=IMUBREAD(0x2B);//Leo el registro CTRL_REG2 0x2B
    CTRL_REG1=CTRL_REG1 | 0x40;//seteo el bit rst 
     __delay_ms(1);
    IMUWRITE(0x2B,CTRL_REG1);// Seteo el bit RST de registro CTRL_REG2
     __delay_ms(1);
    CTRL_REG1=IMUBREAD(0x2B);//Leo el registro CTRL_REG2 0x2B
    CTRL_REG1=CTRL_REG1 & 0xBF;//Limpio el bit rst 
     __delay_ms(1);
    IMUWRITE(0x2B,CTRL_REG1);// Limpio el bit RST de registro CTRL_REG2
      __delay_ms(1);
    MMA8471Activo();
   }
 
 void EnviarMMA(void)
   {
       unsigned int i;
       for (i=0;i<3;i++)
       {
       U1TXREG= VectorRS[i];
       while(!U1STAbits.TRMT);
       }
 for (i=0;i<192;i++)
       {
       U1TXREG= VectorMMA[i];
       while(!U1STAbits.TRMT);
      }
      }
 
void EnviarMMA2(void)
   {
       unsigned int i;
       for (i=0;i<3;i++)
       {
       U2TXREG= VectorRS[i];
       while(!U2STAbits.TRMT);
       }
 for (i=0;i<192;i++)
       {
       U2TXREG= VectorMMA[i];
       while(!U2STAbits.TRMT);
       }
 }

void __attribute__((interrupt, no_auto_psv)) _U1TXInterrupt( void )
 {
}

void __attribute__((interrupt, no_auto_psv)) _SPI2Interrupt( void )
 {
    Nop();
    IFS2bits.SPI2IF=0;
}

void GetTimestamp(FILEIO_TIMESTAMP * timeStamp)//Es para poner fecha y hora en archivo SD
{

    timeStamp->timeMs = 0;
    timeStamp->time.bitfield.hours = tiempo[3];
    timeStamp->time.bitfield.minutes = tiempo[4];
    timeStamp->time.bitfield.secondsDiv2 = tiempo[5];
    timeStamp->date.bitfield.day = tiempo[1];
    timeStamp->date.bitfield.month = tiempo[0];
    timeStamp->date.bitfield.year = 20 + tiempo[2];
    ; //Year (number of years since 1980)
}

void get_int_hex( unsigned int b, char *StrHexa )//Funcion para pasar de int Hexa a string
{
    *StrHexa = nybble_chars[ ( b >> 12 ) & 0x0F ];
    StrHexa++ ;
    *StrHexa = nybble_chars[ (b >>8) & 0x0F ];
    StrHexa++ ;
    *StrHexa = nybble_chars[ ( b >> 4 ) & 0x0F ];
    StrHexa++ ;
    *StrHexa = nybble_chars[ b & 0x0F ];
     StrHexa++ ;
    *StrHexa =';';//separaci9on de punto y coma
}

 
  void __attribute__((interrupt, no_auto_psv)) _U2TXInterrupt( void )
 {
}

  void ClearBloq( unsigned char bloq)// Borrar bloques de medicios
  {
    BloqErase(bloq,0,0)  ; 
  }