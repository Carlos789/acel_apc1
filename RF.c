#include <p33EP512GM304.h>
#define FCY 60000000UL//
#include <libpic30.h>
#include "RF.h"
#include "IMU.h"
#include "MSPI1.h"
#include "UARTSD.h"

extern char cachito[10];
//**********Funciones para SPI1****************************
void RFREAD(char comando){
   CS_RF=0;//bajar chip select
   unsigned int i;
    WByteSPI3(comando);
	
	for(i = 0; i < 5; i++){
		cachito[i] = RByteSPI3();
		}
    CS_RF=1;//Subir chip selecto
}

unsigned char RFBREAD(char reg)//Lectura de un Byte
{
unsigned  char value;
CS_RF=0;
__delay_us(1);// A modo de prueba  
WByteSPI3(reg); 
__delay_us(1);// A modo de prueba    
value=RByteSPI3();
 Nop(); 
__delay_us(1);// A modo de prueba 
CS_RF=1;
return(value);
}


void RFWRITE(char registro, char valor){
    int tempo;
    CS_RF=0;//bajar chip select 
    __delay_us(1);// A modo de prueba  
    WByteSPI3(registro);  
    __delay_us(1);// A modo de prueba  
    WByteSPI3(valor);
    __delay_us(1);// A modo de prueba     
    CS_RF=1;//bajar chip select
}
void RF_TX(char data_RF){
    char aux2;
    RFWRITE(W_CONFIG,0b01011010);//PWR_UP=1, PRIM_RX=0, MASK_TX=0, MASK_RX=0
//    RFWRITE(W_EN_RXADDR,0b00000001);
    //enviar direeccion
    RFBREAD(FLUSH_TX);
    
    __delay_us(10);
    RFWRITE(W_TX_PAYLOAD,data_RF);//cargamos el dato a transmitir
    CE_RF=1;    
    //while ((RFBREAD(R_STATUS)&0b00100000)==0);
    while(IRQ_RF==1);
    //esperamos a que termine la transmision
    RFWRITE(W_STATUS,0b01110000);
    CE_RF=0;
}

char RF_RX(){
    char recep;
    RFWRITE(W_EN_RXADDR,0b00000001);
    RFWRITE(W_CONFIG,0b00111011);//PWR_UP=1, PRIM_RX=1,MASK_TX=0, MASK_RX=0
    RFWRITE(W_RX_PW_P0,0b00000001);//fijando el largo del dato
    CE_RF=1;
    //while ((RFBREAD(R_STATUS)&0b01000000)==0);//esperamos a que termine la transmision
    while(IRQ_RF==1);
    CE_RF=0;
   
    __delay_us(1);
    
    U1TXREG = RFBREAD(R_RX_PW_P0);
    
    RFREAD(R_RX_PAYLOAD);
    U1TXREG = RFBREAD(R_STATUS);
    recep=RFBREAD(R_RX_PAYLOAD);
    RFWRITE(W_STATUS,0b01110000);
    __delay_us(1);// A modo de prueba  
    return(recep);
}